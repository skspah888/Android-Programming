● 자바의 충돌 [ 인터페이스 사용 ]
- 충돌검사 할때마다 사각형을 만드는 것은 좋은 방법이 아니다. ★★
 -> CollisionHelper에서 static으로 선언
1) 충돌 검사할 객체가 담긴 layer를 가져온다.
2) instanceof 기능으로 객체가 맞는지 확인한다. [ Enemy, BoxCollidable 등 으로 ]
3) 충돌검사를 하는 클래스를 만들어, 두개의 객체가 충돌하는지 검사한다.
  -> Util의 CollisionHelper 클래스에서 충돌 검사해서 bool값 return
4) 충돌을 Bullet에서 했으면, Enemy에도 충돌에 대한 수행을 하게 한다.
  -> Bullet의 power를 넘긴 decrease 함수

● 입력 이벤트
- MainActivate에서 입력받는 것은 안좋은 것이다.
- GameView에서 GameWorld로 이벤트를 넘기는 방법을 한다.

● 오브젝트 풀링 [ Recycle ]
- 메모리가 계속 생성/삭제가 된다 => 메모리에 부담이 될 수있다.
- 다 쓴 객체를 버리지않고 재활용 하는 개념 ★★
- 화면상에 사라지는 것은 pool에 넣어놓고, 화면에 보여지는 것은 pool에서 꺼내서 사용
- Pooling하는 객체들은 생성하지 못하게 private으로 생성자를 한다.
 -> 아무나 생성 못하게 한다. 가져오고싶으면 get을 사용하게 한다.


● RecyclePool 클래스
- 각 class 별로 object를 가지고있다.
- 객체를 생성할때, 먼저 이 Pool에 요청하고, 없으면(null)이면 new한다.


★ GameWorld가 객체들을 관리하는 것이 좋다.
- Recycle을 담당하는 class를 만들고, GameWorld가 관리한다.
- GameWorld에서 trash에 넣고 삭제하는 작업을 할때 Pool에 add한다.
 -> tobj가 재활용이 가능한 것들만 Pool에 집어 넣는다.
 -> interface인 recyclable을 상속받은 class들만 pool에 들어갈 수 있게 한다.