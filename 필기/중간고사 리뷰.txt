● 안드로이드 개발에 사용되는 언어
- Java, Kotlin, 스칼라, C
- 버전이름은 간식 이름, 라이센스는 아파치

● Package Name [ Bundle Name ]
- 안드로이드안에 존재하는 모든 것을 구별하는 방법
- 유일한 이름을 부여하여 다른사람과 내것을 구별할 수 있음
- 이게 같으면 같은 프로그램이라 인식, 기존것을 지우고 새로운 것을 덮어 씀
- Reverse Domain

● SDK API Level
- 낮은 레벨은 높은 레벨의 기능을 사용할 수 없음 [ 최신 기능을 사용하지 못함 ]
- API Level이 낮으면 많은 기기에서 사용 가능

● Developer Options [ 개발자 옵션 ]
- 빌드넘버 7번 클릭 등  => 숨겨놨음

● Manifest
- 자바를 jar로 패키지할때 넣어놓는 파일
- 애플리케이션의 속성에 관한 것이 적혀있음

● Gradle Scripts [ ant, maven 등이 있지만, 안드로이드는 build.gradle ]
- 프로그램의 빌드에 관한 것이 적혀있음
- 안드로이드 스튜디오가 빌드라는 과정을 거의 대부분 해줌
- BuildConfig 파일이 생성 => 패키지 이름 등의 정보가 있음
  => 소스코드에서 활용 가능 => 수정은 불가능

● Image Resource Resolution [ mipmap ]
- configuration : 가로모드일때, 세로모드일때, 언어가 바뀜, 화면 회전
 -> 어떤 것들은 제조사가 결정할때 결정되고, 어떤것들은 부팅할때 결정,
     어떤 것은 사용자가 설정할때 결정
 -> mipmap-kr, mipmap-en : 한국어일떄의 이미지와 영어일때의 이미지를 다르게 사용
- mipmap의 - 뒤의 hdpi, mdpi 등의 의미는 configuration의 의미이다. ★
  -> 고해상도일수록 이미지가 작아짐
- value폴더도 마찬가지로 뒤에 -로 configuration 가능 [ res폴더밑의 것들은 모두 가능 ]
- 화면 크기, 언어, 화면 비율, 화면 방향, 화면 픽셀 밀도, 키보드 등
- res폴더는 제일 먼저 컴파일 된다.

● String Resource
- 프로그램 내에서 string은 내부적인 목적으로 사용할때만 사용함 [ 디버깅 등 ]
- 사용자에게 보여줄 메세지가 있으면 따옴표를 사용한 string을 사용하지 않음
- 나중에 수정을 용이하게 하기위함
- 다른 configuration로 옮길 때, 다른 언어을 추가할때 등 편리하게 가능
 -> value-en : 폴더의 string.xml

● 리소스 파일네임 규칙
- 자동으로 생성되는 R.java => Java의 identifier 규칙
- 파일 시스템 규칙이 아닌 Java의 Identifier 규칙을 따름
- 파일의 존재는 가능하지만, 빌드하면 에러발생 [ 저장은 가능, 사용은 X ]
- 한가지 다른점 => 대문자 사용 불가능 [ 소문자, 0~9, _ 만 사용 가능 ]
- 숫자시작 X, 대문자 X, 특수기호(- 등) X

● Layout Managers
- 기기마다 화면의 픽셀, 실제 길이 등이 다르기 때문에 사용
- 적은 리소스를 가지고 많은 기기에 대응하는 방법
- 특정 속성들은 특정 layout에 있을때만 동작. ★
 -> LinearLayout은 weight, RelativeLayout은 below, above, centerInParent
- 서로의 Layout 안에 다른 Layout을 넣을 수 있음. [ 섞어 쓸 수 있다 ]

● 매직넘버 [ 숫자를 직접 주는 방법 ]
- 위치나 관계를 나타낼때는 매직넘버를 사용하지 않음. [ 상황에 따라 달라질 때 ]
- padding [ 어떤 폰에서도 적용할 것 ] 등의 경우는 매직 넘버를 사용
- dimens.xml 형식으로 빼서 사용할 수 있다.

● View ID
- 커맨드 : 화면을 구성하는 View에게 메세지를 전달하는 것 [ 명령을 내린다 ] ★
- notifications[노티피케이션] : view에서 app 으로 메세지를 보내는 것 ★★
 -> 사용자가 어떤 행동을 취했을때 프로그래밍에게 전달되는 것 [ 함수로 연결한다 ] ★
- app이 view에게 명령을 내리는 것을 하기위해 ID 필요
 -> 객체의 레퍼런스를 얻기위한 방법 => findViewById

● findViewById Performance
- 여러번 혹은 루프내에서 호출하는 것은 좋지 않음
 -> 맨위의 View부터 Sub View까지 내려가며 쭉 찾기 때문
- OnCreate 에서 한번한 수행하여 맴버변수에 저장해 놓음

● XML / JSON
- XML : 경우에 따라 드래그앤 드롭보다 더 효율적이고 빠르게 작성할 수 있음
- JSON : 데이터를 주고 받을 때 많이 사용
- XML과 JSON의 구조 ★★★

● setListener() 사용법
- xml: onClick에 함수의 이름을 줘서 부름 
- code : btn.setOnClickListener 사용 [ 넘기는 방법: this, 맴버변수, new로 인라인 ]
- class에서 implements View.onClickListener 하고 this를 넘김
 -> 내가 가지고있는 OnClick함수가 호출됨
 -> 객체마다 다른 함수를 불리게 할 수 없음
 -> 여러개의 버튼에 대한 유연한 대처 불가능 ( 넘어오는 view를 가지고 구별해야함 )

● Logging
- Log.d,e,w,i,v 등 , BreakPoint 

● Single Thread vs Multi Thread
- Java나 Android는 Single Thread 사용
- Android는 MultiThread지원이 조금 미약하다 => Main Thread에서 코드를 작성해야함
- Single Thread : 리소스를 사용할때 Lock, UnLock의 오버헤드가 없음 => 빠름
- Multi Thread : 다운로드 등의 리소스 공유를 사용하는 경우에 이득이 됨

● dimen [ DP, SP, PX ]
- PX는 사용하지 말것 => 기기마다 픽셀이 다르기 때문
- DP : 가장 많이 사용 , SP : 폰트 [ 160dp = 1인치 ]

● Builder Pattern - Alert, View animation
- 세미콜론 한번 안쓰고 속성을 지정하고 보여주기 가능 [ Alert 한 부분 찾아보기 ★ ]
- Alert : 지정한 타이틀, 내용, 확인버튼으로 창 생성, 함수들이 모두 객체를 반환

● Type Inference
- Tool이 이게 무슨 타입인지 알아내는 것 (추론)

● View.tag
- 안드로이드의 모든 view들은 tag를 달 수 있음
 -> tag : 객체형태로 메달수 있고, 모든 view가 가지고있을 수 있음

● Margin / Padding
- background를 적용하면 Margin까지 적용되는지, Padding까지 적용되는지 조사하기 ★
- Padding : 백그라운드에서 떨어짐
- Margin : 전체 영역에서 떨어짐 [ 백그라운드 까지 포함 ]

● Refactor - rename, move, extract(var, const, method), push-down / pull-up

● Style
- 어떤 뷰 들이 같은 속성을 사용한다 => 스타일로 뺀다. [ 공통된것을 묶는다 ]

● Edit Text hint ( placeholder )
- hint : 사용자가 아무 입력안했을때 흐릿한 색깔로 보여주는 것

● ValueAnimator, ObjectAnimator, XML View Animation, Frame Animation
- Frame : 이미지 여러장을 준비해서 시간을 조정해 보여줌, 이미지
- XML : 어떤 View의 위치, 색, 속성을 몇에서 몇으로 , 반복은 몇번 등을 지정
- Value : 값이 변화하는 것에 대해 범용적으로 사용가능 [ 어디서든 ]
- Object : Object의 속성을 직접 바꿔줌

● Object Animator
- 오브젝트 애니메이터에의해 Target에있는 " " 변수가 변화한다.
 -> setXXX 이라는 함수가 존재해야한다.
- Duration : 변화되는 시간 [ 생성할떄 지정한 value 값을 변화시킨다. ]
- setInterpolator(new 종류) : 종류에 따른 중간값으로 계산
 -> Bound, Accel, Anticipate 등
- Start(): 시작
- 계속 new하기 싫으면 한번 new 하고, setIntValues로 불릴 순간에 값 지정


● Interpolater => 점점 빨라지게 등의 옵션  [ Fighter에서 사용 ]

● Game Loop : postDelayed, VSync, Thread
- postDelayed : 일정 시간마다 계속 호출하게 함
- VSync : 화면의 위부터 아래까지 쭉 읽어내는 것 [ Choreographer ]

● PERMISSION : sdcard write, internet, dial, contacts, pictures 
- 사용자에게 허가 받는 것
 -> Manifest에서 <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>

● Sound Play : MediaPlayer, SoundPool
- MediaPlayer : 게임에서 사용하는 것이 적당하지 않음 [ Media객체를 추가함 ]
- SoundPool : resId를 주고  SoundID, SoundID를 가지고 StreamID
 -> - 하나의 리소스를 로드하여 여러개의 스트림을 만들 수 있음

● Configuration change
- 실행 중에 바뀌는 Configuration이 있음 [ 언어는 껏다 킴, 화면전환은 즉시 ]
- Activity가 멈췄다가 다시 실행됨 => 내가 처리할게 가능 ( 다시 실행 안되게 )
- orientaion은 내가 설정할테니 다시 뛰우지마

● Object Recycling, new in game loop
- 게임 루프안에서 객체를 생성하지 마라 => 미리 일정량 생성하고 재활용

● Tiling, Parallex
- 이미지를 직접 그릴것이냐, 이미지뷰에게 맡길 것인가
- 패럴렉스 스크롤링 : 2개이상의 BG를 다른속도로 줘서 2.5차원의 효과

● Canvas push/pop (save/restore) : stack 형태

● SharedPreferences : 값 저장/읽기 [ HighScore ]
1) context에서 prefs를 가져옴 [ 저장할 파일 이름, 모드 설정 ]
2) prefs에서 editor를 가져옴
3) put으로 저장 [ Key, value 기입 ]  
4) commit으로 저장완료
- 가져오는건 get

● enum - ordinal (enum/integer conversion)

● Activity Lifecycle, Intent, Actions
- create -> resume -> running -> pause -> destroy
- https://developer.android.com/guide/components/activities/activity-lifecycle
- onCreate() : Activity가 실행될때 불리는 함수
- onPause / onResume : 다른 Activity를 띄우고 사라질때 

● 인텐트 및 인텐트 필드 
- 어떤 의도를 가지고 어떤 것을 실행하게 할 것인가??
 -> 프로그램에서는 class를 인자로 넘기는 식을 사용함
- 내가 만든 프로그램도 어떤 액션에 의해 실행되게 등록할 수 있음

ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ








