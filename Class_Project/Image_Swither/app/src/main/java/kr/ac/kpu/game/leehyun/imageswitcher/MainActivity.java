package kr.ac.kpu.game.leehyun.imageswitcher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int pageNumber;
    private static int[] IMAGE_RES_IDS = {
            R.mipmap.cat1, R.mipmap.cat2, R.mipmap.cat3, R.mipmap.cat4, R.mipmap.cat5
    };
    private ImageView mainImageView;
    private TextView headerTextView;
    private String headerFormatString;
    private ImageButton prevButton;
    private ImageButton nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainImageView = findViewById(R.id.mainImageView);
        headerTextView = findViewById(R.id.headerTextView);
        prevButton = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);

        Resources res = getResources();
        headerFormatString = res.getString(R.string.header_title_fmt);

        pageNumber = 1;
        showPage();
    }

    public void onBtnPrev(View view) {
//        if (pageNumber <= 1) return;
        --pageNumber;
//        if(pageNumber == 1)
//        {
//            prevButton.setEnabled(false);
//        }
        showPage();
    }

    public void onBtnNext(View view) {
//        if (pageNumber >= IMAGE_RES_IDS.length) return;
        ++pageNumber;
//        if(pageNumber == IMAGE_RES_IDS.length)
//        {
//            nextButton.setEnabled(false);
//        }
        showPage();
    }

    private void showPage() {
        prevButton.setEnabled(pageNumber > 1);
        nextButton.setEnabled(pageNumber < IMAGE_RES_IDS.length);
        // 이미지 바꾸기
        int resId = IMAGE_RES_IDS[pageNumber - 1];
        mainImageView.setImageResource(resId);

        // 타이틀 Set
        String text = String.format(headerFormatString, pageNumber, IMAGE_RES_IDS.length);
        headerTextView.setText(text);
    }


}
