package kr.ac.kpu.game.leehyun.blocksample.game.iface;

public interface ScrollableObject {
    public void scrollTo(int x, int y);
}
