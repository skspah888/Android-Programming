package kr.ac.kpu.game.leehyun.blocksample.game.obj;

import android.content.res.Resources;
import android.graphics.Canvas;

import kr.ac.kpu.game.leehyun.blocksample.R;
import kr.ac.kpu.game.leehyun.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.leehyun.blocksample.res.bitmap.FrameAnimationBitmap;

public class Ball implements GameObject {
    private static final String TAG = Ball.class.getSimpleName();
    public static final int FRAMES_PER_SECOND = 6;
    private final FrameAnimationBitmap fab;
    private final int halfSize;
    private float dx;
    private float dy;
    private float x;
    private float y;

    // 생성자
    public Ball(Resources res, float x, float y, float dx, float dy)
    {
        fab = new FrameAnimationBitmap(R.mipmap.fireball_128_24f, FRAMES_PER_SECOND, 0);
        halfSize = fab.getHeight() / 2;

        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    public void update()
    {
        GameWorld gw = GameWorld.get();
        x += dx;
        if(dx > 0 && x > gw.getRight() - halfSize || dx < 0 &&  x < gw.getLeft() + halfSize)
        {
            dx *= -1;
        }

        y += dy;
        if(dy > 0 && y > gw.getBottom() - halfSize || dy < 0 &&  y < gw.getTop() + halfSize)
        {
            dy *= -1;
        }
        //Log.d(TAG,"Index = " + index);
    }

    @Override
    public void draw(Canvas canvas)
    {
        // canvas와 그릴 위치만 넘겨주면 됨
        fab.draw(canvas, x , y);


//        int index = indexTimer.getIndex();
//        int size = halfSize * 2;
//        Rect rectSrc = new Rect(size * index, 0, (size * (index + 1)), size);
//        RectF rectDst = new RectF(x - halfSize, y - halfSize, x + halfSize, y + halfSize);
//        canvas.drawBitmap(bitmap, rectSrc, rectDst, null);
//        //canvas.drawBitmap(bitmap, x - halfSize, y - halfSize, null);
    }
}
