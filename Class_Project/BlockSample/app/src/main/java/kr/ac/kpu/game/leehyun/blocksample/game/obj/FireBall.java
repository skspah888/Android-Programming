package kr.ac.kpu.game.leehyun.blocksample.game.obj;

import android.content.res.Resources;
import android.graphics.Canvas;

import kr.ac.kpu.game.leehyun.blocksample.R;
import kr.ac.kpu.game.leehyun.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.leehyun.blocksample.res.bitmap.FrameAnimationBitmap;

public class FireBall implements GameObject {
    private static final String TAG = FireBall.class.getSimpleName();
    public static final int FRAMES_PER_SECOND = 10;
    public static final int FRAME_COUNT_FIRE = 2;
    public static final int FRAME_COUNT_FLY = 6;
    private final FrameAnimationBitmap fabFire;
    private final FrameAnimationBitmap fabFly;
    //    private final int halfSize;
    private final int speed;
    private float x;
    private float y;

    public void Fire() {
        if (state != State.Fire)
            return;

        state = State.Fly;
        fabFly.reset();

    }

    private enum State {Fire, Fly}

    private State state = State.Fire;

    // 생성자
    public FireBall(float x, float y, int speed) {
        GameWorld gw = GameWorld.get();
        Resources res = gw.getResources();
        fabFire = new FrameAnimationBitmap(R.mipmap.hadoken1, FRAMES_PER_SECOND, FRAME_COUNT_FIRE);
        fabFly = new FrameAnimationBitmap(R.mipmap.hadoken2, FRAMES_PER_SECOND, FRAME_COUNT_FLY);

//        halfSize = fabFire.getHeight() / 2;

        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    @Override
    public void update() {
        x += speed;

        GameWorld gw = GameWorld.get();
        if (x > gw.getRight()) {
            gw.remove(this);
            return;
        }

        if (state == State.Fire) {
            boolean done = fabFire.done();
            if (done) {
                state = State.Fly;
                fabFly.reset();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        // canvas와 그릴 위치만 넘겨주면 됨
        if (state == State.Fire) {
            fabFire.draw(canvas, x, y);
        } else {
            fabFly.draw(canvas, x, y);
        }


    }
}
