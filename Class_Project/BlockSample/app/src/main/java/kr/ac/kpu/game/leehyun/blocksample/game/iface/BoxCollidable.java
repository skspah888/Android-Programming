package kr.ac.kpu.game.leehyun.blocksample.game.iface;

import android.graphics.RectF;

public interface BoxCollidable {
    public void getBox(RectF rect);
}
