package kr.ac.kpu.game.leehyun.blocksample.game.iface;

public interface Recyclable {
    public void recycle();
}
