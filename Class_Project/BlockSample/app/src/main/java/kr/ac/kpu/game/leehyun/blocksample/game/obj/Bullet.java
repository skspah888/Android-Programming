package kr.ac.kpu.game.leehyun.blocksample.game.obj;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.blocksample.R;
import kr.ac.kpu.game.leehyun.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.Recyclable;
import kr.ac.kpu.game.leehyun.blocksample.game.world.MainWorld;
import kr.ac.kpu.game.leehyun.blocksample.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.blocksample.res.bitmap.FrameAnimationBitmap;

public class Bullet implements GameObject, BoxCollidable, Recyclable {
    private static final String TAG = Bullet.class.getSimpleName();
    public static final int FRAMES_PER_SECOND = 6;
    public static final int FRAME_COUNT = 13;
    private static final float BULLET_SPEED = 1500;
    private FrameAnimationBitmap fab;
    private int halfSize;
    private float x;
    private float y;
    private int power;

    private Bullet() {
        Log.d(TAG, "new: " + this);
    }

    public static Bullet get(float x, float y)
    {
        GameWorld gw = GameWorld.get();
        Resources res = gw.getResources();

        Bullet b = (Bullet) gw.getRecyclePool().get(Bullet.class);
        if(b == null) {
            b = new Bullet();
        }

        b.fab = new FrameAnimationBitmap(R.mipmap.metal_slug_missile, FRAMES_PER_SECOND, FRAME_COUNT);
        b.halfSize = b.fab.getHeight() / 2;

        b.x = x;
        b.y = y;
        b.power = 100;

        return b;
    }

    @Override
    public void update()
    {
        MainWorld gw = MainWorld.get();

        y -= BULLET_SPEED * gw.getTimeDiffInSecond();

        boolean toBeDeleted = false;

        ArrayList<GameObject> enemies = gw.objectsAt(MainWorld.Layer.enemy);
        for (GameObject e : enemies) {
            if (!(e instanceof Enemy))
            {
                Log.e(TAG, "Object at Layer.enemy is: " + e);
                continue;
            }
            Enemy enemy = (Enemy) e;
            if(CollisionHelper.collides(enemy, this)) {
                enemy.decreaseLife(this.power);
                toBeDeleted = true;
                break;
            }
        }

        if(!toBeDeleted) {
            if (y < gw.getTop() - halfSize) {
                toBeDeleted = true;
            }
        }
        //Log.d(TAG,"Index = " + index);

        if(toBeDeleted) {
            gw.remove(this);
        }
    }

    @Override
    public void draw(Canvas canvas)
    {
        // canvas와 그릴 위치만 넘겨주면 됨
        fab.draw(canvas, x , y);


//        int index = indexTimer.getIndex();
//        int size = halfSize * 2;
//        Rect rectSrc = new Rect(size * index, 0, (size * (index + 1)), size);
//        RectF rectDst = new RectF(x - halfSize, y - halfSize, x + halfSize, y + halfSize);
//        canvas.drawBitmap(bitmap, rectSrc, rectDst, null);
//        //canvas.drawBitmap(bitmap, x - halfSize, y - halfSize, null);
    }

    @Override
    public void getBox(RectF rect) {
        int hw = fab.getWidth() / 2;
        int hh = fab.getHeight() / 2;
        rect.left = x - hw;
        rect.right = x + hw;
        rect.top = y - hh;
        rect.bottom = y + hh;
    }

    @Override
    public void recycle() {

    }
}
