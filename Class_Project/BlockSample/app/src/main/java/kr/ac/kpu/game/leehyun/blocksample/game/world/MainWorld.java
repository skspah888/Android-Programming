package kr.ac.kpu.game.leehyun.blocksample.game.world;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;

import kr.ac.kpu.game.leehyun.blocksample.R;
import kr.ac.kpu.game.leehyun.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.Ball;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.EnemyGenerator;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.Fighter;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.Joystick;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.Plane;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.ScoreObject;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.bg.ImageScrollBackground;
import kr.ac.kpu.game.leehyun.blocksample.game.obj.bg.TileScrollBackground;

public class MainWorld extends GameWorld {
    private static final int BALL_COUNT = 10;
    public static final String PREFS_NAME = "Prefs";
    public static final String PREF_KEY_HIGHSCORE = "highScore";
    private static final String TAG = MainWorld.class.getSimpleName();
    private Fighter fighter;
    private EnemyGenerator enemyGenerator = new EnemyGenerator();
    private Plane plane;
    private ScoreObject scoreObject;
    private ScoreObject highscoreObject;
    private PlayState playState = PlayState.normal;
    private Joystick joystick;

    public static void create() {
        if(singleton != null) {
            Log.e(TAG, "Object already created");
        }
        singleton = new MainWorld();
    }

    public static MainWorld get() {
        return (MainWorld) singleton;
    }

    private enum PlayState {
        normal, paused, gameOver
    }

    public enum Layer {
        bg, player, missile, enemy, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void initObjects() {
        Resources res = view.getResources();
//        objects = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < BALL_COUNT; ++i) {
            float x = rand.nextFloat() * 1000;
            float y = rand.nextFloat() * 1000;
            float dx = rand.nextFloat() * 50.f - 25.f;
            float dy = rand.nextFloat() * 50.f - 25.f;

            add(Layer.missile, new Ball(res, x, y, dx, dy));
        }
        float playerY = rect.bottom - 100;

        plane = new Plane(res, 500, playerY, 0.f, 0.f);
        add(Layer.player, plane);

        fighter = new Fighter(200, 700);
        add(Layer.player, fighter);

        scoreObject = new ScoreObject(800, 100, R.mipmap.number_64x84);
        add(Layer.ui, scoreObject);

        highscoreObject = new ScoreObject(800, 20, R.mipmap.number_24x32);
        add(Layer.ui, highscoreObject);


//        add(Layer.bg, new ImageScrollBackground(R.mipmap.bg_city, ImageScrollBackground.Orientation.vertical, 25));
//        add(Layer.bg, new ImageScrollBackground(R.mipmap.clouds, ImageScrollBackground.Orientation.vertical, 100));

        add(Layer.bg, new TileScrollBackground(R.raw.earth, TileScrollBackground.Orientation.vertical, 25));
        add(Layer.bg, new ImageScrollBackground(R.mipmap.clouds, ImageScrollBackground.Orientation.vertical, 100));

//        scorePaint.setTextSize(100);
//        scorePaint.setColor(Color.BLACK);
//        scoreAnimator = ObjectAnimator.ofInt(this, "scoreDisplay",0);

        joystick = new Joystick(300, rect.bottom - 200);
        add(Layer.ui, joystick);

        plane.setJoystick(joystick);

        startGame();
    }

    public void add(Layer layer, final GameObject obj) {
        add(layer.ordinal(), obj);
    }

    public ArrayList<GameObject> objectsAt(Layer layer) {
        return objectsAt(layer.ordinal());
    }

    private void startGame() {
        playState = PlayState.normal;
        scoreObject.reset();

        SharedPreferences prefs = view.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int highscore = prefs.getInt(PREF_KEY_HIGHSCORE, 0);
        highscoreObject.setScore(highscore);
    }

    public void endGame() {
        playState = PlayState.gameOver;
        int score = scoreObject.getScore();

        SharedPreferences prefs = view.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int highscore = prefs.getInt(PREF_KEY_HIGHSCORE, 0);
        if(score > highscore) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(PREF_KEY_HIGHSCORE, score);
            editor.commit();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        joystick.onTouchEvent(event);
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            if (playState == PlayState.gameOver) {
                startGame();
                return false;
            }
            doAction();
//            plane.head(event.getX(), event.getY());
        } else if (action == MotionEvent.ACTION_MOVE) {
//            plane.head(event.getX(), event.getY());
        }
        return true;
    }

    public void doAction() {
        fighter.Fire();
    }

    public void addScore(int score) {
        int value = scoreObject.addScore(score);
    }

    @Override
    public void update(long frameTimeNanos) {
        super.update(frameTimeNanos);

        if (playState != PlayState.normal) {
            return;
        }

        enemyGenerator.update();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
