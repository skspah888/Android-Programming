package kr.ac.kpu.game.leehyun.blocksample.game.obj;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.view.animation.BounceInterpolator;

import kr.ac.kpu.game.leehyun.blocksample.R;
import kr.ac.kpu.game.leehyun.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.leehyun.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.leehyun.blocksample.game.world.MainWorld;
import kr.ac.kpu.game.leehyun.blocksample.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.blocksample.res.sound.SoundEffects;

public class Fighter implements GameObject {
    private static final String TAG = Fighter.class.getSimpleName();
    public static final int FRAMES_PER_SECOND = 10;
    public static final int FRAME_COUNT = 5;
    private final FrameAnimationBitmap fabIdle;
    private final FrameAnimationBitmap fabShoot;
    private final int halfSize;
    private final int shootOffset;
    private float x;
    private float y;

    public void setScale(float scale)
    {
        this.scale = scale;
    }

    private float scale;
//    private long firedOn;

    public void Fire() {
        if (state != State.Idle)
            return;

        // 이 시각에 발사됨
//        firedOn = GameWorld.get().getCurrentTimeNanos();
        ObjectAnimator oa = ObjectAnimator.ofFloat(this,"scale",1.f,2.f);
        oa.setDuration(300);
        oa.setInterpolator(new BounceInterpolator());
        oa.start();

        state = State.Shoot;
        fabShoot.reset();
        SoundEffects.get().play(R.raw.hadouken);
    }

    private void addFireBall() {
        int height = fabIdle.getHeight();
        int fx = (int) (x + height * 0.8f);
        int fy = (int) (y - height * 0.1f);

        int speed = height / 10 ;
        MainWorld gw = MainWorld.get();
        FireBall fb = new FireBall(fx, fy, speed);
        gw.add(MainWorld.Layer.missile, fb);
//        mediaPlayer.start();
    }

    private enum State { Idle, Shoot }

    private State state = State.Idle;

    // 생성자
    public Fighter(float x, float y)
    {
        GameWorld gw = GameWorld.get();
        Resources res = gw.getResources();
        fabIdle = new FrameAnimationBitmap(R.mipmap.ryu, FRAMES_PER_SECOND, 0);
        fabShoot = new FrameAnimationBitmap(R.mipmap.ryu_1, FRAMES_PER_SECOND, FRAME_COUNT);

        // shoot 할때는 shoot의 32%만큼 오른쪽으로 땡겨서 배치
        shootOffset = fabShoot.getHeight() * 32 / 100;
        halfSize = fabIdle.getHeight() / 2;

        this.x = x;
        this.y = y;
        Context context = gw.getContext();
//        this.mediaPlayer = MediaPlayer.create(context, R.raw.hadouken);
    }

    @Override
    public void update()
    {
        if(state == State.Shoot)
        {
            boolean done = fabShoot.done();
            if(done)
            {
                state = State.Idle;
                fabIdle.reset();
                addFireBall();
            }
        }
    }

    @Override
    public void draw(Canvas canvas)
    {
        // canvas와 그릴 위치만 넘겨주면 됨
        if(state == State.Idle)
        {
            fabIdle.draw(canvas, x , y);
        }
        else
        {
            float nowTime = GameWorld.get().getCurrentTimeNanos();
//            float scale = (float) (1 + (nowTime - firedOn) / 1_000_000_000.0);
            canvas.save();
            canvas.scale(scale,scale, x, y);
            fabShoot.draw(canvas, x + shootOffset, y);
            canvas.restore();
        }


    }
}
