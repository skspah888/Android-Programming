package kr.ac.kpu.game.leehyun.listsample.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Serializer {

    public static final String PREFS_NAME = "Highsocre";
    private static final String PREFS_KEY = "scores";
    private static final String TAG = Serializer.class.getSimpleName();

    public static void save(Context context, ArrayList<HighscoreItem> items) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        // jsonString : 배열 items를 json으로 변환
        String jsonString = convertJson(items) ;
        Log.d(TAG, "JSON= " + jsonString);
        editor.putString(PREFS_KEY, jsonString);
        editor.commit();
    }

    private static String convertJson_old(ArrayList<HighscoreItem> items) {
        // StringBuilder는 Therad-safe하지 않다
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        String comma = "";
        for(HighscoreItem item : items)
        {
            sb.append(comma);
            sb.append(item.toJsonString());
            comma = ",";
        }
        sb.append("]");
        return sb.toString();
    }

    private static String convertJson(ArrayList<HighscoreItem> items) {
        JSONArray jarr = new JSONArray();
        for(HighscoreItem item : items) {
            jarr.put(item.toJsonObject());
        }
        return jarr.toString();
    }

    public static ArrayList<HighscoreItem> load(Context context) {
        ArrayList<HighscoreItem> items = new ArrayList<>();

        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String jsonString = prefs.getString(PREFS_KEY, "[]");
        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            int count = jsonArray.length();
            for(int i=0; i<count; i++) {
                JSONObject s = jsonArray.getJSONObject(i);
                HighscoreItem item = new HighscoreItem(s);
                items.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }
}
