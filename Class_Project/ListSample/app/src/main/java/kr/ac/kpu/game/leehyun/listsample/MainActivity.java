package kr.ac.kpu.game.leehyun.listsample;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import kr.ac.kpu.game.leehyun.listsample.data.HighscoreItem;
import kr.ac.kpu.game.leehyun.listsample.data.Serializer;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<HighscoreItem> scores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
        Locale.setDefault(Locale.KOREA);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        scores.add(new HighscoreItem("가가가", new Date(), 2340));
//        scores.add(new HighscoreItem("나나나", new Date(), 1340));
//        scores.add(new HighscoreItem("다다다", new Date(), 31221));
//        scores.add(new HighscoreItem("라라라", new Date(), 874));
//        scores.add(new HighscoreItem("마마마", new Date(), 745));
//        scores.add(new HighscoreItem("바바바", new Date(), 1136));
//        scores.add(new HighscoreItem("사사사", new Date(), 33434));
//        scores.add(new HighscoreItem("아아아", new Date(), 67478));
//        scores.add(new HighscoreItem("자자자", new Date(), 34));

        scores = Serializer.load(this);

        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

    BaseAdapter adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return scores.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //Log.d(TAG, "Pos : " + position + " convertView : " + convertView);
            View view = convertView;
            if(view == null) {
                Log.d(TAG,"Loading from XML for : " + position);
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.score_item, null);
            }

            HighscoreItem s = scores.get(position);
            TextView nameTV = view.findViewById(R.id.nameTextView);
            TextView dataTV = view.findViewById(R.id.dateTextView);
            TextView scoreTV = view.findViewById(R.id.scoreTextView);
            ImageView imageIV = view.findViewById(R.id.imageView);


//            SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//            String formatDate = sdfNow.format(s.date);


            String dstr = getLocalTimeString(s.date);

            nameTV.setText(s.name);
            dataTV.setText(dstr);
            scoreTV.setText(String.valueOf(s.score));
            imageIV.setImageResource(s.res);
            return view;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    };

    private String getLocalTimeString(Date date) {
        String fmt = "yyyy-MM-dd [HH:mm:ss]";
        SimpleDateFormat df = new SimpleDateFormat(fmt);
        df.setTimeZone(TimeZone.getTimeZone("KST"));
        return df.format(date);
    }

    public void onBtnAdd(View view) {
        // Toast
        // Toast.makeText(this, R.string.add_highscore_message, Toast.LENGTH_LONG).show();

        // PhoneCall
        String strUri = "tel:0";
        Uri uri = Uri.parse(strUri);
        Intent intent = new Intent(Intent.ACTION_DIAL, uri);
        startActivity(intent);

//        final EditText et = new EditText(this);
//        new AlertDialog.Builder(this)
//                .setTitle(R.string.highscore)
//                .setMessage(R.string.add_highscore_message)
//                .setView(et)
//                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String name = et.getText().toString();
//                        int score = new Random().nextInt(100000);
//                        addHighscore(name, score);
//                    }
//                })
//                .setNegativeButton(R.string.cancel, null)
//                .create()
//                .show();
    }

    private void addHighscore(String name, int score) {
        scores.add(new HighscoreItem(name, new Date(), score));
        Serializer.save(this, scores);
        //listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void onBtnDelete(View view) {
    }
}
