package kr.ac.kpu.game.leehyun.listsample.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Random;

import kr.ac.kpu.game.leehyun.listsample.R;

public class HighscoreItem {
    public String name;
    public Date date;
    public int score;
    public int res;

    private static final int[] IMAGE_RES_IDS = {
            R.mipmap.card_00, R.mipmap.card_01, R.mipmap.card_02, R.mipmap.card_03,
            R.mipmap.card_04, R.mipmap.card_05, R.mipmap.card_06, R.mipmap.card_07
    };

    public HighscoreItem(String name, Date date, int score) {
        this.name = name;
        this.date = date;
        this.score = score;
        Random random = new Random();
        this.res = IMAGE_RES_IDS[random.nextInt(IMAGE_RES_IDS.length)];
    }

    public HighscoreItem(JSONObject s) throws JSONException {
        this.name = s.getString("name");
        long datevalue = s.getLong("date");
        this.date = new Date(datevalue);
        this.score = s.getInt("score");
        this.res = s.getInt("res");
    }

    public String toJsonString() {
        return "{\"name\":\"" + name + "\", \"date\":" + date.getTime() + ",\"score\":" + score +
                ",\"res\":" + res + "}";
    }

    public JSONObject toJsonObject() {
        JSONObject s = new JSONObject();
        try {
            s.put("name", name);
            s.put("date", date.getTime());
            s.put("score", score);
            s.put("res", res);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return s;
    }
}

// 이렇게 만들어 준다
//{
//    "name": "asd",
//        "date" : 3243
//        "score": 1200
//}
