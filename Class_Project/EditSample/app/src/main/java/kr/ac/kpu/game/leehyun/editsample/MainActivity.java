package kr.ac.kpu.game.leehyun.editsample;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView textView;
    private Animation rotateAnim;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PieView pieview = new PieView(this);
        //setContentView(pieview);
        FrameLayout parent = findViewById(R.id.pieParent);
        parent.addView(pieview);

        editText = findViewById(R.id.editText);
        textView = findViewById(R.id.textView);
        editText.setOnEditorActionListener(editActionListener);

        final TextView animTextView = findViewById(R.id.animTextView);

        animTextView.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable cat = (AnimationDrawable)animTextView.getBackground();
                cat.start();
            }
        });

        // 로드 한 애니메이션 객체를 아무 뷰에나 지정할 수 있음
        rotateAnim = AnimationUtils.loadAnimation(this, R.anim.rotate);

        // 핸들러
        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.v("MainActivity", "Later");
            }
        });

        Log.v("MainActivity", "Who first?");

    }
    private TextView.OnEditorActionListener editActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            String text = editText.getText().toString();
            textView.setText("With Editor Action: " + text);
            return false;
        }
    };

    public void onBtnPush(View view) {
        String text = editText.getText().toString();
        textView.setText("You entered " + text);
        textView.startAnimation(rotateAnim);
    }
}
