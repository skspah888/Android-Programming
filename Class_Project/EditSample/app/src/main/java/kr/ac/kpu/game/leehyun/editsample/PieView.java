package kr.ac.kpu.game.leehyun.editsample;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

public class PieView extends View {
    private static final String TAG = PieView.class.getSimpleName();
    private float rectX;

    public PieView(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
        {
            return false;
        }

        // 100에서 500까지 2초동안 작동한다. 그 값을 가져와서 그린다.
        ValueAnimator anim = ValueAnimator.ofFloat(100, 500);
        anim.setDuration(2000);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                rectX = (float)animation.getAnimatedValue();
                invalidate(); // draw함수가 불린다.
            }
        });
        anim.start();
        return true;
    };

    @Override
    protected void onDraw(Canvas canvas) {
        int w = getWidth();
        int h = getHeight();
        int pl = getPaddingLeft();
        int pt = getPaddingTop();
        Log.d(TAG, "onDraw()");

        Resources res = getResources();
        Bitmap b = BitmapFactory.decodeResource(res, R.mipmap.cat1);
        Matrix m = new Matrix();
        m.preScale(2.f, 1.5f);
        m.preTranslate(100.f, 200.f);
        m.preRotate(2.4f);

        canvas.drawBitmap(b,m,null);
//        canvas.drawBitmap(b, 30, 40, null);

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5.f);
        canvas.drawLine(pl,pt,w,h, paint);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(Color.MAGENTA);
            canvas.drawRoundRect(rectX,30,rectX + w/2, h/2, 60, 60, paint);
        }

        paint.setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL ));
        paint.setTextSize(40);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText("Hello", 3 * w / 4, h / 4, paint);




        super.onDraw(canvas);
    }
}
