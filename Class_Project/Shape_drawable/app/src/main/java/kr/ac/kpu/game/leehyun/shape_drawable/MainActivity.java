package kr.ac.kpu.game.leehyun.shape_drawable;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    private AnimationDrawable ani;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ImageView 객체 참조
        img = findViewById(R.id.img);

        // ImageView에 src 속성으로 설정된 pink_run 이미지를 Drawable 객체로 얻어오기.
        // Frame Animation은 Drawable 일종이므로 형변환 가능
        ani = (AnimationDrawable) img.getDrawable();

        // 처음 시작하면 Run 애니메이션 실행
        // pink.run.xml 리소스는 oneshot이 아니라서 무한반복
        ani.start();

    }

    // onClick 속성이 지정된 View가 클릭되었을 때 자동으로 호출되는 콜백 메소드
    public void mOnClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_run:
                // RUN START 버튼을 눌렀을때 이전 FrameAnimation이 진행중이면 정지
                // Frame Animation은 한번 start() 해주면 계속 Running 상태임
                // Frame Animation은 OneShot으로 하고 다시 시작하게 하고 싶다면
                // 이전 Frame Animation을 stop 시켜야함
                if(ani.isRunning())
                    ani.stop();

                // ImageView에 리소스 파일 셋팅
                img.setImageResource(R.drawable.pink_run);

                // ImageView는 설정된 xml 리소스 파일을 Drawable 객체로 가지고 있으므로 이를 얻어옴
                ani = (AnimationDrawable) img.getDrawable();

                // AnimationDrawable 객체에게 Frame 변경을 시작하도록 함
                ani.start();
                break;

            case R.id.btn_jump:
                if(ani.isRunning())
                    ani.stop();

                img.setImageResource(R.drawable.pink_jump);

                ani = (AnimationDrawable) img.getDrawable();

                ani.start();

                break;

            case R.id.btn_attack:
                if(ani.isRunning())
                    ani.stop();

                img.setImageResource(R.drawable.pink_attack);

                ani = (AnimationDrawable) img.getDrawable();

                ani.start();

                break;

        }
    }
}
