package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;

public class StageUI extends BitmapObject {
    private static final String TAG = StageUI.class.getSimpleName();
    private final Paint paint;
    private String LifeText;
    private String GoldText;
    private String WaveText;
    private float TextX, TextY;


    public StageUI(int life, int gold, int wave) {
        super(UiBridge.x(135), UiBridge.y(20), -70, -70, R.mipmap.state_ui);
        GameData.StageGold = gold;
        GameData.StageLife = life;
        GameData.StageCurWave= 0;
        GameData.StageMaxWave= wave;

        TextY = UiBridge.y(15);

        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(40.f);
        paint.setColor(Color.WHITE);

        LifeText = Integer.toString(GameData.StageLife);
        GoldText = Integer.toString(GameData.StageGold);
        WaveText = " Wave " + Integer.toString(GameData.StageCurWave) + " / " + Integer.toString(GameData.StageMaxWave);
    }

    @Override
    public void update() {
        MakeText();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        super.draw(canvas);
        paint.setTextSize(40.f);
        paint.setColor(Color.WHITE);
        TextX = UiBridge.x(50);
        canvas.drawText(LifeText, TextX, TextY + UiBridge.y(10), paint);

        paint.setTextSize(40.f);
        paint.setColor(Color.YELLOW);
        TextX = UiBridge.x(105);
        canvas.drawText(GoldText, TextX, TextY + UiBridge.y(10), paint);

        paint.setTextSize(35.f);
        paint.setColor(Color.WHITE);
        TextX = UiBridge.x(210);
        canvas.drawText(WaveText, TextX, TextY + UiBridge.y(10), paint);
        canvas.restore();
    }

    public void MakeText() {
        LifeText = Integer.toString(GameData.StageLife);
        GoldText = Integer.toString(GameData.StageGold);
        WaveText = " Wave " + Integer.toString(GameData.StageCurWave) + " / " + Integer.toString(GameData.StageMaxWave);
    }
}
