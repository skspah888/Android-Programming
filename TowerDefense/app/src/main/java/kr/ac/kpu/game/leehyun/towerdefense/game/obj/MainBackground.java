package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;

public class MainBackground extends BitmapObject {
    public MainBackground(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
    }
}
