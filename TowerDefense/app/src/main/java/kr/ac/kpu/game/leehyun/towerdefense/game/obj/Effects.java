package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;

public class Effects extends AnimObject implements Recyclable {

    public Effects(float x, float y, int resId, int fps, int count) {
        super(x, y, 0, 0, resId, fps, count);
    }

    @Override
    public void update() {
        if(fab.done()) {
            remove();
        }
    }

    @Override
    public void recycle() {

    }
}
