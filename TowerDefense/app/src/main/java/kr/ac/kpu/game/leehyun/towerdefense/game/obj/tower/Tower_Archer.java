package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.Canvas;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc.Archer;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.TowerUpgrade;

public class Tower_Archer extends Tower {
    private Archer Archer_Left, Archer_Right;

    public Tower_Archer(float x, float y) {
        super(x, y, 0, 0, R.mipmap.archer_level1, 0, 1);

        FAB_ONE = new FrameAnimationBitmap(R.mipmap.archer_level1, 0, 1);
        FAB_ONE.SetFrameSpeed(0);
        FAB_TWO = new FrameAnimationBitmap(R.mipmap.archer_level3, 0, 1);
        FAB_TWO.SetFrameSpeed(0);
        FAB_THREE = new FrameAnimationBitmap(R.mipmap.archer_level2, 0, 1);
        FAB_THREE.SetFrameSpeed(0);
        FAB_FOUR = new FrameAnimationBitmap(R.mipmap.archer_level4, 0, 1);
        FAB_FOUR.SetFrameSpeed(0);

        Initialize(x,y);
    }

    public static Tower_Archer get(float x, float y) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Tower_Archer item = (Tower_Archer) rpool.get(Tower_Archer.class);
        if (item == null) {
            item = new Tower_Archer(x, y);
        } else {
            item.Initialize(x, y);
        }
        return item;
    }


    @Override
    public void Initialize(float x, float y) {
        this.x = x;
        this.y = y;

        Archer_Left = new Archer(x - UiBridge.x(7),y - UiBridge.y(16), 0, 0, R.mipmap.archer1_right, 5,5);
        Archer_Right = new Archer(x + UiBridge.x(10),y - UiBridge.y(16), 0, 0, R.mipmap.archer1_right, 5,5);

        SetTowerState(1);
        getBox(this.CollisionBox);
        NextState = BehaivorState.idle;
        fab.SetFrameSpeed(0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    if (GameScene.getUiScene() == null && GameScene.getTop() != null) {
                        GameScene.getTop().AddUiScene(new TowerUpgrade(x, y, this));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void SetTowerState(int Level) {
        TowerLevel = Level;

        switch (TowerLevel) {
            case 1:
                fab = FAB_ONE;
                TowerAttack = 8;
                TowerGold = 100;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                Archer_Left.SetArcherState(TowerLevel,TowerAttack);
                Archer_Right.SetArcherState(TowerLevel,TowerAttack);
                SoundEffects.get().play(R.raw.archertower_01, 0);
                break;
            case 2:
                fab = FAB_TWO;
                TowerAttack = 14;
                TowerGold = 200;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                Archer_Left.SetArcherState(TowerLevel,TowerAttack);
                Archer_Right.SetArcherState(TowerLevel,TowerAttack);
                SoundEffects.get().play(R.raw.archertower_02, 0);
                break;
            case 3:
                fab = FAB_THREE;
                TowerAttack = 22;
                TowerGold = 300;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                Archer_Left.SetArcherState(TowerLevel,TowerAttack);
                Archer_Right.SetArcherState(TowerLevel,TowerAttack);
                SoundEffects.get().play(R.raw.archertower_03, 0);
                break;
            case 4:
                fab = FAB_FOUR;
                TowerAttack = 32;
                TowerGold = 400;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                Archer_Left.SetArcherState(TowerLevel,TowerAttack);
                Archer_Right.SetArcherState(TowerLevel,TowerAttack);
                SoundEffects.get().play(R.raw.archertower_04, 0);
                break;
            default:
                break;
        }
    }

    @Override
    public void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case attack:
                UpdateAttack();
                break;
            case COUNT:
                break;
        }
    }

    @Override
    public void CheckState() {
        if(CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    @Override
    public void UpdateIdle() {


    }

    @Override
    public void UpdateAttack() {

    }

    @Override
    public boolean UpgradeTower() {
        // 플레이어 돈 차감
        if(super.UpgradeTower()) {
            TowerLevel += 1;
            if (TowerLevel > TowerMaxLevel) {
                TowerLevel = TowerMaxLevel;
                return false;
            }
            SetTowerState(TowerLevel);
        }

        return true;
    }

    @Override
    public void recycle() { }

    @Override
    public void update() {
        CheckState();
        UpdateState();
        Archer_Left.update();
        Archer_Right.update();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Archer_Right.draw(canvas);
        Archer_Left.draw(canvas);
    }
}
