package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.manager.EffectManager;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.TowerBase;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

public class MeteorSkill extends BitmapObject implements BoxCollidable {
    private RectF CollisionBox = new RectF();

    private BitmapObject Shadow00;
    private BitmapObject Shadow01;
    private BitmapObject Shadow02;

    private PointF GoalPoint = new PointF();
    private float Speed = 0.f;
    private int iEvent = 0;

    public MeteorSkill(float x, float y, float goalx, float goaly) {
        super(x, y, -130, -130, R.mipmap.meteor_00);

        Shadow00 = new BitmapObject(x, y - UiBridge.y(20), -130, -130, R.mipmap.meteor_01);
        Shadow01 = new BitmapObject(x, y - UiBridge.y(35), -130, -130, R.mipmap.meteor_02);
        Shadow02 = new BitmapObject(x, y - UiBridge.y(50), -130, -130, R.mipmap.meteor_03);

        GoalPoint.x = goalx;
        GoalPoint.y = goaly;

        Speed = 600.f;
        iEvent = 0;

        getBox(CollisionBox);
        SoundEffects.get().play(R.raw.fire, 0);
    }

    @Override
    public void getBox(RectF rect) {
        int hw = (int) ((int) sbmp.getWidth() * 1.5f);
        int hh = (int) ((int) sbmp.getHeight()* 1.5f);
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    @Override
    public void update() {
        if (iEvent == -1) {
            // 이펙트 소환
            EffectManager.get().AddEffect(GameScene.getTop(), new Effects(x, y, R.mipmap.effect_bomb, 18, 18));

            // 몬스터 데미지 주기
            SearchMonster();
            SoundEffects.get().play(R.raw.meteor_bomb, 0);

            // 제거
            Shadow00.remove();
            Shadow01.remove();
            Shadow02.remove();
            remove();
        }

        float fValue = Speed * GameTimer.getTimeDiffSeconds();
        Shadow00.SetPos(0.f, fValue);
        Shadow01.SetPos(0.f, fValue);
        Shadow02.SetPos(0.f, fValue);
        y += fValue;

        // Goal 검사
        if (y > GoalPoint.y) {
            iEvent = -1;
        }

    }

    @Override
    public void draw(Canvas canvas) {
        if(iEvent == -1)
            return;

        super.draw(canvas);
        Shadow00.draw(canvas);
        Shadow01.draw(canvas);
        Shadow02.draw(canvas);
    }

    private void SearchMonster() {
        GameScene Scene = GameScene.getTop();
        if (Scene == null)
            return;

        if (Scene instanceof FirstStage) {
            ArrayList<GameObject> enemyList = Scene.getGameWorld().objectsAtLayer(FirstStage.Layer.enemy.ordinal());
            for (GameObject obj : enemyList) {
                if (!(obj instanceof Monster)) {
                    continue;
                }
                Monster monster = (Monster) obj;
                getBox(CollisionBox);
                if (CollisionHelper.collides(CollisionBox, monster)) {
                    monster.DecreaseHp(30);
                }
            }
        }
    }

}
