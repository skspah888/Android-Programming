package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.WindowChange;

public class LoadingWindow extends BitmapObject implements BoxCollidable {
    private static final String TAG = LoadingWindow.class.getSimpleName();
    public static boolean CheckWindow = false;
    public static boolean OpenStage = false;
    private boolean bChange;
    private float speed;
    private float dir;
    private float TimeDeleta;
    private boolean StopTime;
    private Direction WindowDir;

    public enum Direction {
        LEFT, RIGHT, COUNT
    }

    public LoadingWindow(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
        speed = 1000.f;
        dir = 1.f;
        bChange = false;
        TimeDeleta = 0.f;
        StopTime = false;
        OpenStage = false;
    }

    public void SetDirection(Direction direction) {
        switch (direction) {
            case LEFT:
                dir = 1.f;
                x = 0 - UiBridge.metrics.center.x;
                y = UiBridge.metrics.center.y;
                SetSize(x, y, width, height);
                break;

            case RIGHT:
                dir = -1.f;
                x = UiBridge.metrics.size.x + UiBridge.metrics.center.x;
                y = UiBridge.metrics.center.y;
                SetSize(x, y, width, height);
                break;

            case COUNT:
                break;
        }

        WindowDir = direction;
    }


    @Override
    public void update() {
        if (CheckWindow)
            return;

        if(!StopTime)
        {
            //move(dir*speed, 0);
            x += dir * speed * GameTimer.getTimeDiffSeconds();;
        }

        else {
            TimeDeleta += GameTimer.getTimeDiffSeconds();
            if(TimeDeleta > 1.f) {
                StopTime = false;
                TimeDeleta = 0.f;
                OpenStage = true;
            }
        }
        checkItemCollision();

        // 화면 열림
        if(bChange) {
            if(WindowDir == Direction.LEFT) {
                if(dstRect.right < 0.f) {
                    CheckWindow = true;
                    return;
                }
            }
            else if (WindowDir == Direction.RIGHT)
            {
                if(dstRect.left > UiBridge.metrics.size.x) {
                    CheckWindow = true;
                    return;
                }
            }
        }
    }

    private void checkItemCollision() {
        //ArrayList<GameObject> items = WindowChange.getTop().getGameWorld().objectsAtLayer(WindowChange.Layer.ui.ordinal());
        ArrayList<GameObject> items = WindowChange.get().getGameWorld().objectsAtLayer(WindowChange.Layer.ui.ordinal());
        for (GameObject obj : items) {
            if (!(obj instanceof LoadingWindow) || obj == this) {
                continue;
            }
            LoadingWindow window = (LoadingWindow) obj;
            if (CollisionHelper.collides(this, window)) {
                TimeDeleta += GameTimer.getTimeDiffSeconds();
                if (!bChange) {
                    bChange = true;
                    dir = -dir;
                    StopTime = true;
                }
            }
        }
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw + 10.f;
        rect.top = y - hh;
        rect.right = x + hw - 10.f;
        rect.bottom = y + hh;
    }
}
