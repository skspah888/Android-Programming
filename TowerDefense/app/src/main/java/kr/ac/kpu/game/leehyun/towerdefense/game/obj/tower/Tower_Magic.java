package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.RectF;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc.Magic;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.TowerUpgrade;

public class Tower_Magic extends Tower {

    private Monster TargetMonster = null;
    private RectF DetectBox = new RectF();
    private float CoolTime = 0.f;
    private float Time = 0.f;
    private boolean IsCoolTime = false;

    public Tower_Magic(float x, float y) {
        super(x, y, 0, 0, R.mipmap.magic_level1, 5, 5);

        FAB_ONE = new FrameAnimationBitmap(R.mipmap.magic_level1, 5, 5);
        FAB_ONE.SetFrameSpeed(0);
        FAB_TWO = new FrameAnimationBitmap(R.mipmap.magic_level2, 5, 5);
        FAB_TWO.SetFrameSpeed(0);
        FAB_THREE = new FrameAnimationBitmap(R.mipmap.magic_level3, 5, 5);
        FAB_THREE.SetFrameSpeed(0);
        FAB_FOUR = new FrameAnimationBitmap(R.mipmap.magic_level4, 20, 20);
        FAB_FOUR.SetFrameSpeed(0);

        SetTowerState(1);
        getBox(CollisionBox);
        getDetectBox(DetectBox);
    }

    public static Tower_Magic get(float x, float y) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Tower_Magic item = (Tower_Magic) rpool.get(Tower_Magic.class);
        if (item == null) {
            item = new Tower_Magic(x, y);
        } else {
            item.Initialize(x, y);
        }
        return item;
    }


    @Override
    public void Initialize(float x, float y) {
        this.x = x;
        this.y = y;
        SetTowerState(1);
        getBox(this.CollisionBox);
        NextState = BehaivorState.idle;
        fab.SetFrameSpeed(0);
        getDetectBox(DetectBox);
        Random random = new Random();
        CoolTime = random.nextFloat();
        IsCoolTime = false;
        Time = 0.f;
    }

    public void getDetectBox(RectF rect) {
        float hw = fab.getWidth() * 5.f;
        float hh = fab.getHeight() * 5.f;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    if (GameScene.getUiScene() == null && GameScene.getTop() != null) {
                        GameScene.getTop().AddUiScene(new TowerUpgrade(x, y, this));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void SetTowerState(int Level) {
        TowerLevel = Level;

        switch (TowerLevel) {
            case 1:
                fab = FAB_ONE;
                TowerAttack = 10;
                TowerGold = 100;
                TowerSpeed = 150;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.magicower_01, 0);
                break;
            case 2:
                fab = FAB_TWO;
                TowerAttack = 20;
                TowerGold = 200;
                TowerSpeed = 220;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.magictower_02, 0);
                break;
            case 3:
                fab = FAB_THREE;
                TowerAttack = 30;
                TowerSpeed = 300;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.magictower_03, 0);
                break;
            case 4:
                fab = FAB_FOUR;
                TowerAttack = 40;
                TowerSpeed = 400;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.magictower_04, 0);
                break;
            default:
                break;
        }
    }

    @Override
    public void update() {
        CheckState();
        UpdateState();
    }

    @Override
    public void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case attack:
                UpdateAttack();
                break;
            case COUNT:
                break;
        }
    }

    @Override
    public void CheckState() {
        if (CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    @Override
    public void UpdateIdle() {
        if(IsCoolTime) {
            Time += GameTimer.getTimeDiffSeconds();
            if(Time >= CoolTime) {
                IsCoolTime = false;
                Time = 0.f;
            }
            return;
        }

        SearchMonster();

        // 적을 찾았으면 Attack 상태로
        if(TargetMonster != null) {
            NextState = BehaivorState.attack;
            fab.ResetFrameSpeed();
        }
    }

    @Override
    public void UpdateAttack() {
        // 찾은 적이 없으면 State를 Idle 상태로
        SearchMonster();

        if(TargetMonster == null) {
            NextState =  BehaivorState.idle;
            fab.SetFrameSpeed(0);
            return;
        }
        else {
            // 적 위치에 따라 방향 변경
            if(fab.done()) {
                // 미사일 발사
                GameScene Scene = GameScene.getTop();
                if(Scene != null) {
                    if(Scene instanceof FirstStage) {
                        Scene.getGameWorld().add(FirstStage.Layer.object.ordinal(), new Magic(x,y - UiBridge.y(10), TowerLevel, TowerAttack, TowerSpeed, TargetMonster));
                        NextState = BehaivorState.idle;
                        TargetMonster = null;
                        IsCoolTime = true;
                        SoundEffects.get().play(R.raw.magicattack, 0);
                    }
                }
            }
        }

    }

    private void SearchMonster() {
        GameScene Scene = GameScene.getTop();
        if(Scene == null)
            return;

        if(Scene instanceof FirstStage) {
            ArrayList<GameObject> enemyList = Scene.getGameWorld().objectsAtLayer(FirstStage.Layer.enemy.ordinal());
            for (GameObject obj : enemyList) {
                if (!(obj instanceof Monster)) {
                    continue;
                }
                Monster monster = (Monster) obj;
                if (CollisionHelper.collides(DetectBox, monster)) {
                    if(TargetMonster == null) {
                        TargetMonster = monster;
                        return;
                    }  else {
                        // 거리 비교해서 더 가까운 몬스터 등록
                        float targetX = TargetMonster.getX() - this.x;
                        float targetY = TargetMonster.getY() - this.y;
                        float targetDist = (float) Math.sqrt((targetX * targetX + targetY * targetY));

                        float dx = monster.getX() - this.x;
                        float dy = monster.getY() - this.y;
                        float fDist = (float) Math.sqrt((dx * dx + dy * dy));

                        if(fDist < targetDist) {
                            TargetMonster = monster;
                            return;
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean UpgradeTower() {
        // 플레이어 돈 차감
        if(super.UpgradeTower()) {
            TowerLevel += 1;
            if (TowerLevel > TowerMaxLevel) {
                TowerLevel = TowerMaxLevel;
                return false;
            }
            SetTowerState(TowerLevel);
        }

        return true;
    }

    @Override
    public void recycle() {

    }
}
