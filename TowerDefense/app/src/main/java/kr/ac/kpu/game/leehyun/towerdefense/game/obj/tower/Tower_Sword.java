package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.TowerUpgrade;

public class Tower_Sword extends Tower {
    public Tower_Sword(float x, float y) {
        super(x, y, 0, 0, R.mipmap.sword_level1, 3, 3);

        FAB_ONE = new FrameAnimationBitmap(R.mipmap.sword_level1, 3, 3);
        FAB_ONE.SetFrameSpeed(0);
        FAB_TWO = new FrameAnimationBitmap(R.mipmap.sword_level2, 4, 4);
        FAB_TWO.SetFrameSpeed(0);
        FAB_THREE = new FrameAnimationBitmap(R.mipmap.sword_level3, 4, 4);
        FAB_THREE.SetFrameSpeed(0);
        FAB_FOUR = new FrameAnimationBitmap(R.mipmap.sword_level4, 4, 4);
        FAB_FOUR.SetFrameSpeed(0);

        SetTowerState(1);
        getBox(CollisionBox);
    }

    public static Tower_Sword get(float x, float y) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Tower_Sword item = (Tower_Sword) rpool.get(Tower_Sword.class);
        if (item == null) {
            item = new Tower_Sword(x, y);
        } else {
            item.Initialize(x, y);
        }
        return item;
    }



    @Override
    public void Initialize(float x, float y) {
        this.x = x;
        this.y = y;
        SetTowerState(1);
        getBox(this.CollisionBox);
        NextState = BehaivorState.idle;
        fab.SetFrameSpeed(0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    captureTouch();
                    if(GameScene.getUiScene() == null && GameScene.getTop() != null) {
                        GameScene.getTop().AddUiScene(new TowerUpgrade(x,y,this));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void SetTowerState(int Level) {
        TowerLevel = Level;

        switch (TowerLevel) {
            case 1:
                fab = FAB_ONE;
                TowerAttack = 10;
                TowerGold = 100;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.swordtower_01, 0);
                break;
            case 2:
                fab = FAB_TWO;
                TowerAttack = 20;
                TowerGold = 200;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.swordtower_02, 0);
                break;
            case 3:
                fab = FAB_THREE;
                TowerAttack = 30;
                TowerGold = 300;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.swordtower_03, 0);
                break;
            case 4:
                fab = FAB_FOUR;
                TowerAttack = 40;
                TowerGold = 400;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.swordtower_04, 0);
                break;
            default:
                break;
        }
    }

    @Override
    public void update() {
    }

    @Override
    public void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case attack:
                UpdateAttack();
                break;
            case COUNT:
                break;
        }
    }

    @Override
    public void CheckState() {
        if(CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    @Override
    public void UpdateIdle() {


    }

    @Override
    public void UpdateAttack() {

    }

    @Override
    public boolean UpgradeTower() {
        // 플레이어 돈 차감
        if(super.UpgradeTower()) {
            TowerLevel += 1;
            if (TowerLevel > TowerMaxLevel) {
                TowerLevel = TowerMaxLevel;
                return false;
            }
            SetTowerState(TowerLevel);
        }

        return true;
    }

    @Override
    public void recycle() {

    }
}
