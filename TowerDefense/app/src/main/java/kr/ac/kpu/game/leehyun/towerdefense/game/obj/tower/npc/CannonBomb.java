package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc;

import android.graphics.PointF;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.manager.EffectManager;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.Effects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.Tower_Cannon;

public class CannonBomb extends BitmapObject implements Recyclable {

    private static final float GRAVITY_POWER = -1500;
    private static final float GRAVITY_SPEED = 4500;
    private PointF Direction = new PointF();

    private SharedBitmap L1_Bomb;
    private SharedBitmap L2_Bomb;
    private SharedBitmap L3_Bomb;
    private SharedBitmap L4_Bomb;

    private Monster TargetMonster;
    private PointF GoalPoint = new PointF();
    private int BombLevel = 0;
    private int BombAttack = 0;
    private int speed = 0;
    private int BombSpeed = 0;
    private int iEvent = 0;
    private int EffectRestId;
    private int EffectFPS;
    private int EffectCount;

    private boolean IsUp = false;

    public CannonBomb(float x, float y, int level, int attack, int speed, Monster monster) {
        super(x, y, 0, 0, R.mipmap.cannon_bomb1);

        L1_Bomb = SharedBitmap.load(R.mipmap.cannon_bomb1);
        L2_Bomb = SharedBitmap.load(R.mipmap.cannon_bomb2);
        L3_Bomb = SharedBitmap.load(R.mipmap.cannon_bomb3);
        L4_Bomb = SharedBitmap.load(R.mipmap.cannon_bomb4);

        Initialize(x, y, level, attack, speed, monster);
    }

    public static CannonBomb get(float x, float y, int level, int attack, int speed, Monster monster) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        CannonBomb item = (CannonBomb) rpool.get(CannonBomb.class);
        if (item == null) {
            item = new CannonBomb(x, y, level, attack, speed, monster);
        } else {
            item.Initialize(x, y, level, attack, speed, monster);
        }
        return item;
    }

    private void Initialize(float x, float y, int level, int attack, int speed, Monster monster) {
        BombLevel = level;
        BombAttack = attack;
        BombSpeed = speed;
        TargetMonster = monster;
        GoalPoint.x = TargetMonster.getX();
        GoalPoint.y = TargetMonster.getY();
        this.speed = 0;

        switch (BombLevel) {
            case 1:
                EffectRestId = R.mipmap.cannon_effect1;
                EffectFPS = 10;
                EffectCount = 10;
                break;
            case 2:
                sbmp = L2_Bomb;
                EffectRestId = R.mipmap.cannon_effect2;
                EffectFPS = 11;
                EffectCount = 11;
                break;
            case 3:
                sbmp = L3_Bomb;
                EffectRestId = R.mipmap.cannon_effect3;
                EffectFPS = 11;
                EffectCount = 11;
                break;
            case 4:
                sbmp = L4_Bomb;
                EffectRestId = R.mipmap.cannon_effect3;
                EffectFPS = 11;
                EffectCount = 11;
                break;
        }

        this.speed += GRAVITY_POWER;
        if (this.speed > GRAVITY_POWER) {
            this.speed = (int) GRAVITY_POWER;
        }
        IsUp = false;
    }

    @Override
    public void update() {
        if (iEvent == -1) {
            if (TargetMonster != null)
                TargetMonster.DecreaseHp(BombAttack);
            EffectManager.get().AddEffect(GameScene.getTop(), new Effects(x, y, EffectRestId, EffectFPS, EffectCount));
            remove();
            return;
        }

        // 위로 올라갔다가 아래로 내려가야함
        y += speed * GameTimer.getTimeDiffSeconds();
        speed += GRAVITY_SPEED * GameTimer.getTimeDiffSeconds();
        x += Direction.x * 1000 * GameTimer.getTimeDiffSeconds();

        if(speed > 0 )
            IsUp = true;

        UpdatePoint();
        UpdateDirection();
    }

    private void UpdatePoint() {
        if (TargetMonster != null) {
            GoalPoint.x = TargetMonster.getX();
            GoalPoint.y = TargetMonster.getY();
        }
    }

    private void UpdateDirection() {
        // 목적지를 기반으로 Direction 설정
        PointF CurPos = new PointF(x, y);
        Direction.x = (GoalPoint.x - CurPos.x);
        Direction.y = (GoalPoint.y - CurPos.y);
        float Length = Direction.length();
        Direction.x = Direction.x / Length;
        Direction.y = Direction.y / Length;

        float dx = GoalPoint.x - this.x;
        float dy = GoalPoint.y - this.y;
        float fDist = (float) Math.sqrt((dx * dx + dy * dy));

        if (IsUp && y > GoalPoint.y) {
            iEvent = -1;
        }

//        if(fDist < 15.f) {
//            iEvent = -1;
//        }
    }

    @Override
    public void recycle() {

    }
}
