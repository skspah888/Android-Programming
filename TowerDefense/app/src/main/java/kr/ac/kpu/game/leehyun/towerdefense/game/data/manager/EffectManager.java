package kr.ac.kpu.game.leehyun.towerdefense.game.data.manager;

import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.Effects;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

// 몬스터 이동 경로를 가지고 있는 라인 매니저
public class EffectManager {
    private static final String TAG = EffectManager.class.getSimpleName();
    private static EffectManager singleton;

    public static EffectManager get() {
        if(singleton == null) {
            singleton = new EffectManager();
        }
        return singleton;
    }

    public void AddEffect(GameScene CurrentScene, GameObject EffectObject) {
        if(CurrentScene == null)
            return;

        if(CurrentScene instanceof FirstStage) {
            CurrentScene.getGameWorld().add(FirstStage.Layer.effect.ordinal(),EffectObject);
        } else if(CurrentScene instanceof SecondStage) {
            CurrentScene.getGameWorld().add(SecondStage.Layer.effect.ordinal(),EffectObject);
        }
    }

}
