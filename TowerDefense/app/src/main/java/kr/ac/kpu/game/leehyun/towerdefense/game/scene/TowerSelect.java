package kr.ac.kpu.game.leehyun.towerdefense.game.scene;

import android.animation.ValueAnimator;
import android.view.MotionEvent;
import android.view.animation.OvershootInterpolator;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TowerFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.SelectTowerUI;

public class TowerSelect extends GameScene {
    private static final String TAG = TowerSelect.class.getSimpleName();
    private final TowerFlag Towerflag;
    private float BaseX, BaseY;
    private SelectTowerUI SelectUI;

    public enum Layer {
        bg , ui  , COUNT
    }

    public TowerSelect(float x, float y, TowerFlag towerFlag) {
        BaseX = x;
        BaseY = y;
        Towerflag = towerFlag;
    }

    @Override
    protected int getLayerCount() {
        return TowerSelect.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        Initialize();

        // 오버슛 인터폴 사용해서 애니메이션 효과 넣어보기
        ValueAnimator anim = ValueAnimator.ofFloat(0.f, 1.f);
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float)animation.getAnimatedValue();
                ScaleTo(value);
            }
        });
        anim.start();
    }

    private void ScaleTo(float value) {
        SelectUI.SetScale(value);
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        // SelectTowerUI 생성
        SelectUI = new SelectTowerUI(BaseX , BaseY,-80,-80, Towerflag);
        gameWorld.add(Layer.ui.ordinal(), SelectUI);
        SoundEffects.get().play(R.raw.click_00, 0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(!super.onTouchEvent(event)) {
            DeleteUiScene();
            return true;
        }
        return false;
    }
}
