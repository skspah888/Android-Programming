package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.LoadingWindow;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.MainMenu;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SeletMap;

public class WindowChange extends GameScene {
    private static final String TAG = WindowChange.class.getSimpleName();
    public SceneLayer SceneState;
    public StackLayer StackState;
    public boolean OpenLevel;
    private static WindowChange instance;

    public WindowChange(SceneLayer sceneState, StackLayer stackState) {
        SceneState = sceneState;
        StackState = stackState;
        OpenLevel = false;
        SoundEffects.get().play(R.raw.windowmove, 0);

    }

    public enum SceneLayer {
        Menu, Select, FirstStage, SecondStage, COUNT
    }

    public enum StackLayer {
        Back, Front, COUNT
    }

    public enum Layer {
        bg, ui,  COUNT
    }

    @Override
    protected int getLayerCount() {
        return WindowChange.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();

        // 문이 닫혔을 때
        if(LoadingWindow.OpenStage) {
            if(!OpenLevel) {
                UpdateScene();
                LoadingWindow.OpenStage = false;
                OpenLevel = true;
                SoundEffects.get().play(R.raw.windowclose, 0);
            }
        }

        // 문이 닫혔다가 열렸을 때
        if(LoadingWindow.CheckWindow) {
            LoadingWindow.CheckWindow = false;
            SoundEffects.get().play(R.raw.windowopen, 0);
            DeleteUiScene();
        }
    }

    private void UpdateScene() {
        // 뒤로 가기 [ new가 아닌 pop ]
        if(StackState == StackLayer.Back) {
            GameScene.topGameScene.pop();
        }

        // 앞으로 가기 [ new ]
        else if (StackState == StackLayer.Front) {
            switch (SceneState) {
                case Menu:
                    new MainMenu().push();
                    break;
                case Select:
                    new SeletMap().push();
                    break;
                case FirstStage:
                    SeletMap.StopSound();
                    new FirstStage().push();
                    break;
                case SecondStage:
                    SeletMap.StopSound();
                    new SecondStage().push();
                    break;
                case COUNT:
                    break;
            }
        }
    }

    @Override
    public void enter() {
        super.enter();
        Initialize();
        instance = this;
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        // 왼쪽창, 오른쪽창 생성
        // Left window
        LoadingWindow LeftWindow = new LoadingWindow(cx, cy, 0, 0, R.mipmap.loading_left);
        LeftWindow.SetDirection(LoadingWindow.Direction.LEFT);
        gameWorld.add(Layer.ui.ordinal(),LeftWindow);

        // Right window
        LoadingWindow RightWindow = new LoadingWindow(cx, cy, 0, 0, R.mipmap.loading_right);
        RightWindow.SetDirection(LoadingWindow.Direction.RIGHT);
        gameWorld.add(Layer.ui.ordinal(),RightWindow);
    }

    public static WindowChange get() {
        return instance;
    }
}
