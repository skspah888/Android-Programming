package kr.ac.kpu.game.leehyun.towerdefense.framework.obj;

import android.graphics.Canvas;
import android.graphics.RectF;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;

public class BitmapObject extends GameObject implements BoxCollidable {
    private static final String TAG = BitmapObject.class.getSimpleName();
    protected SharedBitmap sbmp;
    protected final RectF dstRect;
    protected int width;
    protected int height;

    public BitmapObject(float x, float y, int width, int height, int resId) {
        this.x = x;
        this.y = y;
        this.Ax = x;
        this.Ay = y;
        this.dstRect = new RectF();

        if(resId == 0)
            return;

        sbmp = SharedBitmap.load(resId);
        if (width == 0) {
            width = UiBridge.x(sbmp.getWidth());
        } else if (width < 0) {
            width = UiBridge.x(sbmp.getWidth()) * -width / 100;
        }
        this.width = width;
        if (height == 0) {
            height = UiBridge.x(sbmp.getHeight());
        } else if (height < 0) {
            height = UiBridge.x(sbmp.getHeight()) * -height / 100;
        }
        this.height = height;
    }

    public void SetSize(float x, float y, int width, int height)
    {
        this.x = x;
        this.y = y;
        if (width == 0) {
            width = UiBridge.x(sbmp.getWidth());
        } else if (width < 0) {
            width = UiBridge.x(sbmp.getWidth()) * -width / 100;
        }
        this.width = width;
        if (height == 0) {
            height = UiBridge.x(sbmp.getHeight());
        } else if (height < 0) {
            height = UiBridge.x(sbmp.getHeight()) * -height / 100;
        }
        this.height = height;
    }

    public void AddBitmap(int resId) {
        SharedBitmap.load(resId);
    }

    public SharedBitmap LoadBitmap(int resId) {
        return (sbmp = SharedBitmap.load(resId));
    }

    @Override
    public float getRadius() {
        return this.width / 2;
    }

    public void SetPos(float ValueX, float ValueY) {
        x += ValueX;
        y += ValueY;
    }

    public float getHeight() { return this.height;}
    public float getWidth() { return this.width;}

    public void draw(Canvas canvas) {
        int halfWidth = (int)width / 2;
        int halfHeight = (int)height / 2;
        dstRect.left = x - halfWidth;
        dstRect.top = y - halfHeight;
        dstRect.right = x + halfWidth;
        dstRect.bottom = y + halfHeight;
        canvas.drawBitmap(sbmp.getBitmap(), null, dstRect, null);
    }

    @Override
    public void getBox(RectF rect) {
        int hw = (int)width / 2;
        int hh = (int)height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
