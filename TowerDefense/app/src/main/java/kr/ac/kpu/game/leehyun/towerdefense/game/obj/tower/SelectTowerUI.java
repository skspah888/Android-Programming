package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TowerFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

public class SelectTowerUI extends BitmapObject implements Touchable {
    private static final String TAG = SelectTowerUI.class.getSimpleName();
    private final TowerFlag Towerflag;
    private RectF CollisionBox;
    private GameScene gameScene;
    private boolean IsDead;

    public SelectTowerUI(float x, float y, int width, int height, TowerFlag towerflag) {
        super(x, y, width, height, R.mipmap.select_tower);
        CollisionBox = new RectF();
        getBox(CollisionBox);

        gameScene = GameScene.getTop();
        Towerflag = towerflag;
    }

    private float Value = 0f;

    public void SetScale(float value) {
        Value = value;
    }

    @Override
    public void update() {
        if (IsDead) {
            Towerflag.remove();
            gameScene.DeleteUiScene();
            IsDead = false;
            releaseTouch();
            this.remove();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    int mx = (int) e.getX();
                    int my = (int) e.getY();

                    TowerBase.TowerLayer TowerState = TowerBase.TowerLayer.COUNT;

                    if (mx < x && my < y) {
                        TowerState = TowerBase.TowerLayer.Archer;
                    }

                    if (mx < x && my > y) {
                        TowerState = TowerBase.TowerLayer.Magic;
                    }

                    if (mx > x && my < y) {
                        TowerState = TowerBase.TowerLayer.Cannon;
                    }

                    if (mx > x && my > y) {
                        TowerState = TowerBase.TowerLayer.Sword;
                    }

                    if (TowerState != TowerBase.TowerLayer.COUNT) {
                        if (gameScene instanceof FirstStage) {
                            gameScene.getGameWorld().add(FirstStage.Layer.object.ordinal(), new TowerBase(x, y, TowerState));
                            IsDead = true;
                        } else if(gameScene instanceof SecondStage) {
                            gameScene.getGameWorld().add(SecondStage.Layer.object.ordinal(), new TowerBase(x, y, TowerState));
                            IsDead = true;
                        }
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.scale(Value, Value, x, y);
        super.draw(canvas);
        canvas.restore();
    }
}
