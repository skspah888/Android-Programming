package kr.ac.kpu.game.leehyun.towerdefense.framework.main;

public class GameTimer {
    private static final long NANOS_IN_ONE_SECOND = 1_000_000_000;
    private static final float NANOS_IN_ONE_SECOND_FLOAT = 1_000_000_000f;
    private static long currentTimeNanos;
    private static long diffNanos;
    private static float TimeSpeed = 1f;

    public static void setCurrentTimeNanos(long timeNanos) {
        diffNanos = timeNanos - currentTimeNanos;
        currentTimeNanos = timeNanos;
    }
    public static long getCurrentTimeNanos() {
        return currentTimeNanos * (long) TimeSpeed;
    }
    public static long getTimeDiffNanos() {
        return diffNanos * (long) TimeSpeed;
    }
    public static float getTimeDiffSeconds() {
        return (diffNanos / NANOS_IN_ONE_SECOND_FLOAT) * TimeSpeed;
    }

    public static void SetTimeSpeed(float Value) {
        TimeSpeed = Value;
    }

    protected int count;
    protected int fps;
    protected int Basefps;
    protected long time;

    public GameTimer(int count, int framesPerSecond) {
        this.count = count;
        this.fps = framesPerSecond;
        this.Basefps = framesPerSecond;
        this.time = currentTimeNanos;
    }

    public int getRawIndex() {
        long currentTime = currentTimeNanos;
        long elapsed = currentTime - this.time;
        int index = (int) ((((elapsed * fps + NANOS_IN_ONE_SECOND / 2) / NANOS_IN_ONE_SECOND)) * TimeSpeed);
        if(index > count) {
            this.time = currentTime;
        }
        return index;
    }
    public int getIndex() {
        int index = getRawIndex();
        return index % count;
    }
    public boolean done() {
        int index = getRawIndex();
        return index > count;
    }

    public void reset() {
        this.time = currentTimeNanos;
    }

    public void SetFrameSpeed(int Value) {
        this.fps = Value;
    }

    public void ResetFrameSpeed() {
        this.fps = this.Basefps;
    }
}
