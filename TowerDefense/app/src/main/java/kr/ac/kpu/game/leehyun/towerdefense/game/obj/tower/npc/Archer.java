package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;

public class Archer extends AnimObject implements BoxCollidable {
    public static final String TAG = Archer.class.getSimpleName();
    private RectF CollisionBox;
    public FrameAnimationBitmap L1_BACK;
    public FrameAnimationBitmap L1_RIGHT;
    public FrameAnimationBitmap L2_BACK;
    public FrameAnimationBitmap L2_RIGHT;
    public FrameAnimationBitmap L3_BACK;
    public FrameAnimationBitmap L3_RIGHT;
    public FrameAnimationBitmap L4_BACK;
    public FrameAnimationBitmap L4_RIGHT;


    public ArcherState CurState = ArcherState.COUNT;
    public ArcherState NextState = ArcherState.COUNT;
    public ArcherDir CurDir = ArcherDir.COUNT;
    public ArcherDir NextDir = ArcherDir.COUNT;

    public int Level = 0;
    public int Attack = 0;
    public float Range = 0.f;
    public float ResverseValue = 1.f;

    private Monster TargetMonster = null;

    @Override
    public void getBox(RectF rect) {
        float hw = fab.getWidth() * Range;
        float hh = fab.getHeight() * Range * 1.5f;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    public enum ArcherState {
        idle, attack, COUNT
    }

    public enum ArcherDir {
        left, right, up, down, COUNT
    }

    public Archer(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, height, resId, fps, count);
        fab.SetFrameSpeed(0);

        CollisionBox = new RectF();

        L1_BACK = new FrameAnimationBitmap(R.mipmap.archer1_back, 4, 4);
        L1_BACK.SetFrameSpeed(0);
        L1_RIGHT = new FrameAnimationBitmap(R.mipmap.archer1_right, 5, 5);
        L1_RIGHT.SetFrameSpeed(0);

        L2_BACK = new FrameAnimationBitmap(R.mipmap.archer2_back, 5, 5);
        L2_BACK.SetFrameSpeed(0);
        L2_RIGHT = new FrameAnimationBitmap(R.mipmap.archer2_right, 5, 5);
        L2_RIGHT.SetFrameSpeed(0);

        L3_BACK = new FrameAnimationBitmap(R.mipmap.archer3_back, 5, 5);
        L3_BACK.SetFrameSpeed(0);
        L3_RIGHT = new FrameAnimationBitmap(R.mipmap.archer3_right, 5, 5);
        L3_RIGHT.SetFrameSpeed(0);

        L4_BACK = new FrameAnimationBitmap(R.mipmap.archer4_back, 5, 5);
        L4_BACK.SetFrameSpeed(0);
        L4_RIGHT = new FrameAnimationBitmap(R.mipmap.archer4_right, 5, 5);
        L4_RIGHT.SetFrameSpeed(0);

        SetArcherState(1, 10);
        getBox(CollisionBox);
        NextState = ArcherState.idle;
        NextDir = ArcherDir.right;
    }


    public void SetArcherState(int level, int attack) {
        Level = level;
        Attack = attack;
        Range = 8.f + (float)Level;
    }


    public void UpdateIdle() {
        // 근처 적 찾기 [ Target 지정 ]
        SearchMonster();

        // 적을 찾았으면 Attack 상태로
        if(TargetMonster != null) {
            NextState =  ArcherState.attack;
            UpdateDirection();
            CheckDirection();
            fab.ResetFrameSpeed();
        }
    }
    public void UpdateAttack() {
        // 근처 적 찾기 [ 타겟 변경 ]
        SearchMonster();

        // 찾은 적이 없으면 State를 Idle 상태로
        if(TargetMonster == null) {
            NextState = ArcherState.idle;
            NextDir = ArcherDir.right;
            ResverseValue = 1.f;
            fab.SetFrameSpeed(0);
            return;

        }
        else {
            // 적 위치에 따라 방향 변경
            UpdateDirection();
            CheckDirection();       // 현재 방향 체크 -> 이미지 변경 FSM
            if(fab.done()) {
                if(Level <= 3)
                    SoundEffects.get().play(R.raw.archerattack_bow, 0);
                else
                    SoundEffects.get().play(R.raw.archerattack_gun, 0);
                TargetMonster.DecreaseHp(Attack);
                TargetMonster = null;
            }
        }
    }

    private void SearchMonster() {
        GameScene Scene = GameScene.getTop();
        if(Scene == null)
            return;

        if(Scene instanceof FirstStage) {
            ArrayList<GameObject> enemyList = Scene.getGameWorld().objectsAtLayer(FirstStage.Layer.enemy.ordinal());
            for (GameObject obj : enemyList) {
                if (!(obj instanceof Monster)) {
                    continue;
                }
                Monster monster = (Monster) obj;
                if (CollisionHelper.collides(CollisionBox, monster)) {
                    if(TargetMonster == null) {
                        TargetMonster = monster;
                        return;
                    }
                    else {
                        // 거리 비교해서 더 가까운 몬스터 등록
                        float targetX = TargetMonster.getX() - this.x;
                        float targetY = TargetMonster.getY() - this.y;
                        float targetDist = (float) Math.sqrt((targetX * targetX + targetY * targetY));

                        float dx = monster.getX() - this.x;
                        float dy = monster.getY() - this.y;
                        float fDist = (float) Math.sqrt((dx * dx + dy * dy));

                        if(fDist < targetDist) {
                            TargetMonster = monster;
                            return;
                        }
                    }
                }
            }
        }
    }


    @Override
    public void update() {
        UpdateState();          // 행동 업데이트
        CheckState();           // 현재 상태 체크 -> 행동 변경 FSM
    }

    public void CheckState() {
        if(CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    public void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case attack:
                UpdateAttack();
                break;
            case COUNT:
                break;
        }
    }

    private void CheckDirection() {
        if(CurDir == NextDir)
            return;

        switch (NextDir) {
            case left:
                switch (Level){
                    case 1:
                        fab = L1_RIGHT;
                        break;
                    case 2:
                        fab = L2_RIGHT;
                        break;
                    case 3:
                        fab = L3_RIGHT;
                        break;
                    case 4:
                        fab = L4_RIGHT;
                        break;
                }
                ResverseValue = -1.f;
                break;
            case right: case down:
                switch (Level){
                    case 1:
                        fab = L1_RIGHT;
                        break;
                    case 2:
                        fab = L2_RIGHT;
                        break;
                    case 3:
                        fab = L3_RIGHT;
                        break;
                    case 4:
                        fab = L4_RIGHT;
                        break;
                }
                ResverseValue = 1.f;
                break;
            case up:
                switch (Level){
                    case 1:
                        fab = L1_BACK;
                        break;
                    case 2:
                        fab = L2_BACK;
                        break;
                    case 3:
                        fab = L3_BACK;
                        break;
                    case 4:
                        fab = L4_BACK;
                        break;
                }
                ResverseValue = 1.f;
                break;

            case COUNT:
                break;
        }
        fab.ResetFrameSpeed();
        CurDir = NextDir;
    }

    private void UpdateDirection() {
        // 몬스터 위치와 자신의 위치로 각도 구하기
        double dx = TargetMonster.getX() - this.x;
        double dy = TargetMonster.getY() - this.y;
        double Angle = Math.toDegrees(Math.atan2(dy, dx));

        // 목표와 자신의 위치로 현재 방향 State 변경 및 ReverseValue 변경
        if(Angle > 45 && Angle < 135) {
            // 위
            NextDir = ArcherDir.up;

        } else if ((Angle > 0 && Angle < 45) || (Angle > -45 && Angle < 0)) {
            // 오른쪽
            NextDir = ArcherDir.right;

        } else if (Angle < -45 && Angle > -135) {
            // 아래
            NextDir = ArcherDir.down;

        } else if ((Angle > -180 && Angle < -135) || (Angle > 135 && Angle < 180)) {
            // 왼쪽
            NextDir = ArcherDir.left;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.scale(ResverseValue,1, x, y);
        super.draw(canvas);
        canvas.restore();
    }
}
