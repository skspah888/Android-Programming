package kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.HPBar;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc.Archer;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;

public class Wolf extends Monster {
    private FrameAnimationBitmap FAB_UP;
    private FrameAnimationBitmap FAB_FRONT;
    private FrameAnimationBitmap FAB_RIGHT;
    private FrameAnimationBitmap FAB_DIE;
    private FrameAnimationBitmap FAB_ATTACK;
    private float MoveTime;

    public Wolf(float x, float y, LinkedList<PointF> Line) {
        super(x, y, 0, 0, R.mipmap.wolf_right, 8, 8);

        FAB_UP = new FrameAnimationBitmap(R.mipmap.wolf_up, 5,5);
        FAB_UP.SetFrameSpeed(0);

        FAB_FRONT = new FrameAnimationBitmap(R.mipmap.wolf_front, 5,5);
        FAB_FRONT.SetFrameSpeed(0);

        FAB_RIGHT = new FrameAnimationBitmap(R.mipmap.wolf_right, 8,8);
        FAB_RIGHT.SetFrameSpeed(0);

        FAB_DIE = new FrameAnimationBitmap(R.mipmap.wolf_die, 8,8);
        FAB_DIE.SetFrameSpeed(0);

        FAB_ATTACK = new FrameAnimationBitmap(R.mipmap.wolf_attack, 10,10);
        FAB_ATTACK.SetFrameSpeed(0);

        MonsterAttack = 10;
        MonsterGold = 100;
        MonsterSpeed = 80.f;

        Initialize(x,y,Line);
    }

    public static Wolf get(float x, float y, LinkedList<PointF> Line) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Wolf item = (Wolf) rpool.get(Wolf.class);
        if (item == null) {
            item = new Wolf(x, y,Line);
        } else {
            item.Initialize(x, y, Line);
        }
        return item;
    }

    @Override
    protected void Initialize(float x, float y, LinkedList<PointF> Line) {
        Linelist = Line;
        CurIndex = 0; IsFinish = false;
        MonsterHp = 100;
        iEvent = 0;
        MoveTime = 0.f;

        // 경로 배정
        MaxIndex = Linelist.size();

        // 목적지 배정
        GoalPoint = Linelist.get(CurIndex);

        // 몬스터 위치 지정
        this.x = GoalPoint.x;
        this.y = GoalPoint.y;


        // 탐지 범위 설정 [ BoostMan은 필요 없을 듯 ]
        getDetectBox(DetectBox, 5.f);

        CurDir = MonsterDir.COUNT;
        NextDir = MonsterDir.right;

        CurState = MonsterState.COUNT;
        NextState = MonsterState.idle;

        fab.SetFrameSpeed(0);

        hpbar = HPBar.get(x,y,this);
        SoundEffects.get().play(R.raw.wolf_init, 0);
    }

    @Override
    public void update() {
        if(iEvent == -1) {
            hpbar.remove();
            remove();
            return;
        }

        if(MonsterHp <= 0) {
            NextState = MonsterState.die;
        }

        UpdateState();          // 행동 업데이트
        UpdateDirection();      // 방향 업데이트

        CheckState();           // 현재 상태 체크 -> 행동 변경 FSM
        CheckDirection();       // 현재 방향 체크 -> 이미지 변경 FSM
        hpbar.update();
    }

    @Override
    public void draw(Canvas canvas) {
        if(iEvent == -1) {
            return;
        }

        canvas.save();
        canvas.scale(ReverseValue,1, x, y);
        super.draw(canvas);
        hpbar.draw(canvas);
        canvas.restore();
    }

    @Override
    protected void CheckState() {
        if(CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case move:
                fab.ResetFrameSpeed();
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case die:
                fab = FAB_DIE;
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    @Override
    protected void CheckDirection() {
        if(CurDir == NextDir)
            return;

        switch (NextDir) {
            case left:
                if(CurState == MonsterState.attack)
                    fab = FAB_ATTACK;
                else
                    fab = FAB_RIGHT;
                ReverseValue = -1.f;
                break;
            case right:
                if(CurState == MonsterState.attack)
                    fab = FAB_ATTACK;
                else
                    fab = FAB_RIGHT;
                ReverseValue = 1.f;
                break;
            case up:
                if(CurState == MonsterState.attack)
                    fab = FAB_ATTACK;
                else
                    fab = FAB_UP;
                ReverseValue = 1.f;
                break;
            case down:
                if(CurState == MonsterState.attack)
                    fab = FAB_ATTACK;
                else
                    fab = FAB_FRONT;
                ReverseValue = 1.f;
                break;
            case COUNT:
                break;
        }
        CurDir = NextDir;
    }

    @Override
    protected void UpdateDirection() {
        // 목표 위치와 자신의 위치로 각도 구하기
        double dx = GoalPoint.x - this.x;
        double dy = GoalPoint.y - this.y;
        double Angle = Math.toDegrees(Math.atan2(dy, dx));

        // 목표와 자신의 위치로 현재 방향 State 변경 및 ReverseValue 변경
        if(Angle > 45 && Angle < 135) {
            // 위
            NextDir = MonsterDir.up;

        } else if ((Angle > 0 && Angle < 45) || (Angle > -45 && Angle < 0)) {
            // 오른쪽
            NextDir = MonsterDir.right;

        } else if (Angle < -45 && Angle > -135) {
            // 아래
            NextDir = MonsterDir.down;

        } else if ((Angle > -180 && Angle < -135) || (Angle > 135 && Angle < 180)) {
            // 왼쪽
            NextDir = MonsterDir.left;
        }
    }

    @Override
    protected void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case move:
                UpdateMove();
                break;
            case attack:
                UpdateAttack();
                break;
            case die:
                UpdateDie();
                break;
            case COUNT:
                break;
        }

    }


    @Override
    protected void UpdateIdle() {
        // Wolf Idle이 없고 바로 Move로 감
        NextState = MonsterState.move;

        // 목적지를 통해 방향 만들어 주기
        PointF CurPos = new PointF(x,y);
        Direction.x = (GoalPoint.x - CurPos.x);
        Direction.y = (GoalPoint.y - CurPos.y);
        float Length = Direction.length();
        Direction.x = Direction.x / Length;
        Direction.y = Direction.y / Length;
    }

    @Override
    protected void UpdateMove() {
        // 목적지 근처에 갔으면 목적지 변경 [ index 증가 , index 검사 ]
        fab.ResetFrameSpeed();
        float dx = GoalPoint.x - this.x;
        float dy = GoalPoint.y - this.y;
        float fDist = (float) Math.sqrt((dx * dx + dy * dy));

        if(fDist < 3.f) {
            CurIndex++;
            if(CurIndex >= MaxIndex) {      // 목적지 도달
                IsFinish = true;
                iEvent = -1;
                GameData.StageLife -= 1;
                return;
            }
            GoalPoint = Linelist.get(CurIndex);
        }

        // 목적지를 기반으로 Direction 설정
        PointF CurPos = new PointF(x,y);
        Direction.x = (GoalPoint.x - CurPos.x);
        Direction.y = (GoalPoint.y - CurPos.y);
        float Length = Direction.length();
        Direction.x = Direction.x / Length;
        Direction.y = Direction.y / Length;

        // 목적지로 이동
        x += Direction.x * MonsterSpeed * GameTimer.getTimeDiffSeconds();
        y += Direction.y * MonsterSpeed * GameTimer.getTimeDiffSeconds();
        getDetectBox(DetectBox,5);

//        // 탐지하여 Attack으로 변경?
//        MoveTime += GameTimer.getTimeDiffSeconds();
//        if(MoveTime > 1.f) {
//            SoundEffects.get().play(R.raw.booster_move, 0);
//            MoveTime = 0.f;
//        }
    }

    @Override
    protected void UpdateAttack() {
        if(fab.done()) {
            // 데미지 주기
        }

        // 탐지 범위 벗어나면 Idle 상태로
    }

    @Override
    protected void UpdateDie() {
        if(fab.done()) {
            GameData.StageGold += MonsterGold;
            iEvent = -1;
            SoundEffects.get().play(R.raw.wolf_die, 0);
            SoundEffects.get().play(R.raw.monster_die, 0);
        }
    }

    @Override
    public void recycle() {

    }

    private void DetectObject() {
        ArrayList<GameObject> obstacles = GameScene.getTop().getGameWorld().objectsAtLayer(FirstStage.Layer.npc.ordinal());
        for (GameObject obj : obstacles) {
            // 스워드 인지 검사
            if (!(obj instanceof Archer)) {
                continue;
            }

            Archer obstacle = (Archer) obj;
            if (CollisionHelper.collides(DetectBox, obstacle)) {

            }
        }
    }
}
