package kr.ac.kpu.game.leehyun.towerdefense.framework.util;

import android.graphics.RectF;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;

public class CollisionHelper {
    private static RectF rect1 = new RectF();
    private static RectF rect2 = new RectF();
    public static boolean collides(BoxCollidable o1, BoxCollidable o2) {
        o1.getBox(rect1);
        o2.getBox(rect2);
        if (rect1.left > rect2.right) {
            return false;
        }
        if (rect1.right < rect2.left) {
            return false;
        }
        if (rect1.top > rect2.bottom) {
            return false;
        }
        if (rect1.bottom < rect2.top) {
            return false;
        }
        return true;
    }

    public static boolean collides(BoxCollidable o1, RectF dst) {
        o1.getBox(rect1);
        if (rect1.left > dst.right) {
            return false;
        }
        if (rect1.right < dst.left) {
            return false;
        }
        if (rect1.top > dst.bottom) {
            return false;
        }
        if (rect1.bottom < dst.top) {
            return false;
        }
        return true;
    }

    public static boolean collides(RectF src, RectF dst) {
        if (src.left > dst.right) {
            return false;
        }
        if (src.right < dst.left) {
            return false;
        }
        if (src.top > dst.bottom) {
            return false;
        }
        if (src.bottom < dst.top) {
            return false;
        }
        return true;
    }

    public static boolean collides(RectF src, BoxCollidable o2) {
        o2.getBox(rect2);
        if (src.left > rect2.right) {
            return false;
        }
        if (src.right < rect2.left) {
            return false;
        }
        if (src.top > rect2.bottom) {
            return false;
        }
        if (src.bottom < rect2.top) {
            return false;
        }
        return true;
    }

    public static float getDistanceSquare(GameObject o1, GameObject o2) {
        float dx = o1.getX() - o2.getX();
        float dy = o1.getY() - o2.getY();
        return dx * dx + dy * dy;
    }
    public static float getDistance(GameObject o1, GameObject o2) {
        return (float) Math.sqrt(getDistanceSquare(o1, o2));
    }
}
