package kr.ac.kpu.game.leehyun.towerdefense.game.scene;

import android.animation.ValueAnimator;
import android.view.MotionEvent;
import android.view.animation.OvershootInterpolator;

import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.Tower;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.UpgradeTowerUI;

public class TowerUpgrade extends GameScene {
    private static final String TAG = TowerUpgrade.class.getSimpleName();
    private float BaseX, BaseY;
    private UpgradeTowerUI UpgradeUI;
    private Tower tower;

    public TowerUpgrade(float x, float y, Tower _tower) {
        BaseX = x;
        BaseY = y;
        tower = _tower;
    }

    public enum Layer {
        bg , ui  , COUNT
    }

    @Override
    protected int getLayerCount() {
        return TowerUpgrade.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        Initialize();

        // 오버슛 인터폴 사용해서 애니메이션 효과 넣어보기
        ValueAnimator anim = ValueAnimator.ofFloat(0.f, 1.f);
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float)animation.getAnimatedValue();
                ScaleTo(value);
            }
        });
        anim.start();
    }

    private void ScaleTo(float value) {
        UpgradeUI.SetScale(value);
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        UpgradeUI = new UpgradeTowerUI(BaseX , BaseY,-80,-80, tower);
        gameWorld.add(Layer.ui.ordinal(), UpgradeUI);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(!super.onTouchEvent(event)) {
            DeleteUiScene();
            return true;
        }
        return false;
    }
}
