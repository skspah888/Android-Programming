package kr.ac.kpu.game.leehyun.towerdefense.framework.main;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;

public abstract class GameScene {
    private static final String TAG = GameScene.class.getSimpleName();
    private static ArrayList<GameScene> sceneStack = new ArrayList<>();

    protected static GameScene topGameScene;
    protected static GameScene UiScene;

    public static boolean touchScenes(MotionEvent event) {
        if(UiScene != null)
            return UiScene.onTouchEvent(event);

        return getTop().onTouchEvent(event);
    }

    public static void drawScenes(Canvas canvas) {
        // sceneStack에 있는 Scene부터 그리기
        int topIndex = sceneStack.size() - 1;
        if(topIndex >= 0)
            drawSceneAt(topIndex, canvas);

        // 제일 위에 그려질 Scene 그리기
        if(UiScene != null)
            UiScene.draw(canvas);
    }

    protected static void drawSceneAt(int stackIndex, Canvas canvas) {
        GameScene scene = sceneStack.get(stackIndex);

        // 해당 Scene을 그리는데, 그 Scene이 Transparent하다면 그 밑에 Scene을 그려라라
       if(scene.isTransparent() && stackIndex > 0) {
            drawSceneAt(stackIndex -1, canvas);
        }
        scene.draw(canvas);
    }

    public static void updateScenes() {
        // sceneStack에 있는 Scene부터 업데이트
        int topIndex = sceneStack.size() - 1;
        if(topIndex >= 0)
            updateSceneAt(topIndex);

        if(UiScene != null)
            UiScene.update();
    }

    private static void updateSceneAt(int stackIndex) {
        GameScene scene = sceneStack.get(stackIndex);
        // 해당 Scene을 그리는데, 그 Scene이 Transparent하다면 그 밑에 Scene을 그려라라
        if(scene.isUpdate() && stackIndex > 0) {
            updateSceneAt(stackIndex -1);
        }
        scene.update();
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    protected GameWorld gameWorld;

    public boolean isTransparent() {
        return isTransparent;
    }

    public void setTransparent(boolean transparent) {
        this.isTransparent = transparent;
    }

    protected boolean isTransparent;

    public boolean isUpdate() {
        return IsUpdate;
    }

    public void setUpdate(boolean update) {
        this.IsUpdate = update;
    }

    protected boolean IsUpdate;


    abstract protected int getLayerCount();
    protected GameScene() {
        int layerCount = getLayerCount();
        gameWorld = new GameWorld(layerCount);
    }

    public static GameScene getUiScene() {
        if(UiScene == null) {
            Log.e(TAG, "topGameScene is null on getTop(). Maybe a bug?");
            return null;
        }
        return UiScene;
    }

    public static GameScene getTop() {
        if (topGameScene == null) {
            Log.e(TAG, "topGameScene is null on getTop(). Maybe a bug?");
            return null;
        }
        return topGameScene;
    }
    public void run() {
        if (sceneStack.size() != 0) {
            Log.e(TAG, "sceneStack size is not 0 on run(). Maybe a bug?");
        }
        sceneStack.add(this);
        topGameScene = this;
        this.enter();
    }
    public void push() {
        int size = sceneStack.size();
        if (size > 0) {
            GameScene gw = sceneStack.get(size - 1);
            gw.pause();
        }
        sceneStack.add(this);
        topGameScene = this;
        this.enter();
    }
    public static void pop() {
        int size = sceneStack.size();
        if (size == 0) return;
        GameScene top = sceneStack.get(size - 1);
        top.exit();
        sceneStack.remove(size - 1);
        if (size == 1) {
            UiBridge.exit();
            return;
        }
        topGameScene = sceneStack.get(size - 2);
        topGameScene.resume();
    }
    public void replace() {
        int size = sceneStack.size();
        if (size == 0) {
            Log.e(TAG, "sceneStack size is 0 on replace(). Maybe a bug?");
        }
        GameScene top = sceneStack.get(size - 1);
        top.exit();
        sceneStack.remove(size - 1);
        sceneStack.add(this);
        topGameScene = this;
        this.enter();
    }

    public void draw(Canvas canvas) { gameWorld.draw(canvas); }
    public void update() { gameWorld.update(); }
    public void enter() { Log.v(TAG, "enter() - " + getClass().getSimpleName()); }
    public void exit() { Log.v(TAG, "exit() - " + getClass().getSimpleName()); }
    public void pause() { Log.v(TAG, "pause() - " + getClass().getSimpleName()); }
    public void resume() { Log.v(TAG, "resume() - " + getClass().getSimpleName()); }

    public boolean onTouchEvent(MotionEvent event) {
        return gameWorld.onTouchEvent(event);
    }

    public void onBackPressed() {
        pop();
    }


    public void AddUiScene(GameScene scene) {
        if(UiScene == null) {
            UiScene = scene;
            UiScene.enter();
        }
    }

    public void DeleteUiScene() {
        if(UiScene == null)
            return;

        UiScene.exit();
        UiScene = null;
        System.gc();
    }

}
