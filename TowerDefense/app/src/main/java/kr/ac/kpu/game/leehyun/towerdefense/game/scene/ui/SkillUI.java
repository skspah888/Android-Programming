package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.MotionEvent;

import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MeteorSkill;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

public class SkillUI extends BitmapObject implements Touchable {
    private static final String TAG = SkillUI.class.getSimpleName();
    private final float Speed;
    private final float MaxValue;
    private boolean IsSound;
    private SharedBitmap CooldownBitmap;
    private BitmapObject SkillAngle;
    private AnimObject MeteorCircle;
    private float Cooldown;
    private boolean IsSkill;
    private boolean IsCooldown;
    private RectF DrawBox;
    private Rect CooldownBox;
    private int SoundStream;

    public SkillUI(float x, float y, int width, int height, int resID) {
        super(x,y, width,height, resID);
        CooldownBitmap = SharedBitmap.load(R.mipmap.black_alpha, false);
        SkillAngle = new BitmapObject(x,y,0,0,R.mipmap.skill_ui);
        MeteorCircle = new AnimObject(x,y,0,0,R.mipmap.meteorskill, 39,39);

        CooldownBox = new Rect();
        DrawBox = new RectF();

        CooldownBox.top = (int) (y - SkillAngle.getHeight() * 0.4f);
        CooldownBox.bottom = (int) (y + SkillAngle.getHeight()* 0.4f);
        CooldownBox.left = (int) (x - SkillAngle.getWidth()* 0.4f);
        CooldownBox.right = (int) (x + SkillAngle.getWidth()* 0.4f);

        DrawBox.top = (int) (y - SkillAngle.getHeight()* 0.4f);
        DrawBox.bottom = (int) (y + SkillAngle.getHeight()* 0.4f);
        DrawBox.left = (int) (x - SkillAngle.getWidth()* 0.4f);
        DrawBox.right = (int) (x + SkillAngle.getWidth()* 0.4f);

        MaxValue = CooldownBox.bottom;

        Speed = 50.f;
        IsSkill = false;
        IsCooldown = false;
        Cooldown = 0f;
        IsSound = false;
    }

    @Override
    public void update() {
        updateRect();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(IsCooldown) {
            canvas.drawBitmap(CooldownBitmap.getBitmap(), CooldownBox, DrawBox, null);
        }

        SkillAngle.draw(canvas);

        if(IsSkill) {
            MeteorCircle.draw(canvas);
        }
    }

    public void updateRect() {
        if(!IsCooldown)
            return;

        // Bottom 증가
        DrawBox.top +=  Speed * GameTimer.getTimeDiffSeconds();

        if(DrawBox.top > MaxValue) {
            DrawBox.top = CooldownBox.top;
            IsCooldown = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (CooldownBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    if(!IsCooldown) {
                        IsCooldown = true;
                        IsSkill = true;
                        SoundEffects.get().play(R.raw.meteor_click, 0);
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(IsSkill) {
                    MeteorCircle.SetX(e.getX());
                    MeteorCircle.SetY(e.getY());
                    if(!IsSound) {
                        SoundStream = SoundEffects.get().play(R.raw.skill_move, -1);
                        IsSound = true;
                    }

                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if(IsSkill) {
                    // 해당 위치에 메테오 생성
                    MeteorCircle.SetX(x);
                    MeteorCircle.SetY(y);
                    IsSound = false;
                    SoundEffects.get().StopSound(SoundStream);

                    GameScene Scene = GameScene.getTop();
                    Random random = new Random();
                    for(int i=0; i<12; ++i) {
                        int RandomX = random.nextInt(UiBridge.x(60)) - UiBridge.x(30);
                        int RandomY = random.nextInt(UiBridge.y(120)) - UiBridge.y(60);
                        MeteorSkill Meteor = new MeteorSkill(e.getX()+ RandomX, RandomY, e.getX(),e.getY());
                        if(Scene instanceof FirstStage) {
                            Scene.getGameWorld().add(FirstStage.Layer.effect.ordinal(), Meteor);
                        } else if(Scene instanceof SecondStage) {
                            Scene.getGameWorld().add(SecondStage.Layer.effect.ordinal(), Meteor);
                        }
                    }

                    IsSkill = false;
                    return true;
                }
                break;
        }
        return false;
    }
}
