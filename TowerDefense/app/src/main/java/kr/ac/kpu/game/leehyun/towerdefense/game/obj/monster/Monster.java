package kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster;

import android.graphics.PointF;
import android.graphics.RectF;

import java.util.LinkedList;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.HPBar;

public abstract class Monster extends AnimObject implements BoxCollidable, Recyclable {
    protected static final String TAG = Monster.class.getSimpleName();
    protected MonsterState CurState = MonsterState.COUNT;
    protected MonsterState NextState = MonsterState.COUNT;
    protected MonsterDir CurDir = MonsterDir.COUNT;
    protected MonsterDir NextDir = MonsterDir.COUNT;

    protected LinkedList<PointF> Linelist;
    protected int MaxIndex = 0;
    protected int CurIndex = 0;
    protected RectF DetectBox;
    protected PointF Direction = new PointF();
    protected PointF GoalPoint = new PointF();
    protected int MonsterAttack = 0;
    protected int MonsterGold = 0;
    protected int MonsterHp = 50;
    protected float MonsterSpeed = 0f;
    protected float ReverseValue = 1.f;

    protected boolean IsFinish = false;
    protected int iEvent = 0;

    protected HPBar hpbar;

    public enum MonsterState {
        idle, move , attack, die, COUNT
    }

    public enum MonsterDir {
        left, right , up, down , COUNT
    }

    public int GetAttack() { return MonsterAttack;}
    public int GetGold() { return MonsterGold;}
    public float GetSpeed() { return MonsterSpeed;}
    public float GetHp() { return MonsterHp;}
    public float GetHeight() {return fab.getHeight(); }

    public void SetAttack(int attack) { MonsterAttack = attack;}
    public void SetGold(int gold) { MonsterGold = gold;}
    public void SetSpeed(int speed) { MonsterSpeed = speed;}
    public void SetFinish() { IsFinish = true;}
    public void SetHp(int hp) { MonsterHp = hp;}
    public void DecreaseHp(int hp) { MonsterHp -= hp;}

    public Monster(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, height, resId, fps, count);
        fab.SetFrameSpeed(0);
        DetectBox = new RectF();
        NextState = MonsterState.idle;
    }

    protected abstract void Initialize(float x, float y,LinkedList<PointF> Line);

    protected abstract void CheckState();
    protected abstract void CheckDirection();
    protected abstract void UpdateDirection();
    protected abstract void UpdateState();

    protected abstract void UpdateIdle();
    protected abstract void UpdateMove();
    protected abstract void UpdateAttack();
    protected abstract void UpdateDie();


    @Override
    public void getBox(RectF rect) {
        int hw = (int)fab.getWidth() / 2;
        int hh = (int)fab.getHeight() / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    public void getDetectBox(RectF rect , float value) {
        int hw = (int) (fab.getWidth() * value);
        int hh = (int) (fab.getHeight() * value);
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
