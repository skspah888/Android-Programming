package kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.StageExplanation;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.TipUI;

public class TipMark extends BitmapObject implements Touchable, BoxCollidable {
    private Runnable onClickRunnable;
    private RectF CollisionBox;
    private boolean ImageScale;

    public TipMark(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
        CollisionBox = new RectF();
        getBox(CollisionBox);
    }

    public void setOnClickRunnable(Runnable onClickRunnable) {
        this.onClickRunnable = onClickRunnable;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    ImageScale = true;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (this.CollisionBox.contains((int) e.getRawX(), (int) e.getRawY())) {
                    ImageScale = true;
                } else {
                    ImageScale = false;
                }
                return true;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    ImageScale = false;
                    // UI 띄우기
                    if (onClickRunnable != null) {
                        onClickRunnable.run();
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        if(ImageScale) {
            canvas.save();
            canvas.scale(1.5f,1.5f, x,y );
            super.draw(canvas);
            canvas.restore();
        }
        else {
            super.draw(canvas);
        }
    }
}
