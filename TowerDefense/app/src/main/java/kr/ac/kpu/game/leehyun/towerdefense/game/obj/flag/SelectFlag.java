package kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.StageExplanation;

public class SelectFlag extends Flag  {
    private FlagState flagState;
    private boolean FlagScale;

    public enum FlagState {
        Stage01, Stage02, COUNT
    }

    public SelectFlag(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, height, resId, fps, count);
        FlagScale = false;
    }

    public void Initialize(FlagState eState) {
        flagState = eState;

        onClickRunnable = new Runnable() {
            @Override
            public void run() {
                // FlagState에 따라 어떤 UI Scene을 띄울지
                switch (flagState) {
                    case Stage01:
                        new StageExplanation(flagState, GameData.Stage01_Open).push();
                        break;
                    case Stage02:
                        new StageExplanation(flagState, GameData.Stage02_Open).push();
                        break;
                    case COUNT:
                        break;
                }
            }
        };
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    FlagScale = true;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (this.CollisionBox.contains((int) e.getRawX(), (int) e.getRawY())) {
                    FlagScale = true;
                } else {
                    FlagScale = false;
                }
                return true;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    FlagScale = false;
                    // UI 띄우기
                    if (onClickRunnable != null) {
                        onClickRunnable.run();
                        SoundEffects.get().play(R.raw.click_00, 0);
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        if(FlagScale) {
            canvas.save();
            canvas.scale(1.5f,1.5f, x,y + height/2.f);
            super.draw(canvas);
            canvas.restore();
        }
        else {
            super.draw(canvas);
        }
    }
}
