package kr.ac.kpu.game.leehyun.towerdefense.game.scene;


import android.graphics.Canvas;
import android.media.MediaPlayer;

import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.NinePatchButton;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.tile.TiledMap;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.manager.SpawnManager;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.DecoFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TipMark;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TowerFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.GameBackUI;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.SettingUI;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.SkillUI;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.StageUI;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.StartBattleUI;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.TipUI;

public class SecondStage extends GameScene {
    private static final String TAG = SecondStage.class.getSimpleName();
    private static TiledMap TileMap;
    private StartBattleUI BattleUI;

    private float SpawnTime;
    private float RandomTime;
    private MediaPlayer sound;

    public enum Layer {
        bg, object, enemy, towerobject, npc, effect, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return SecondStage.Layer.COUNT.ordinal();
    }

    private boolean TestBool = false;
    @Override
    public void update() {
        super.update();

        if(GameData.GameResult) {
            pop();
        }

//       if (SpawnManager.get().GetBattle()) {
//           SpawnTime += GameTimer.getTimeDiffSeconds();
//
//           if(SpawnTime > RandomTime) {
//               Random random = new Random();
//               RandomTime = random.nextFloat() + 0.3f;
//               SpawnTime = 0.f;
//               // 몬스터 생성
//               SpawnManager.get().MonsterSpawn(this);
//           }
//       }
    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (GameData.DrawTile) {
            TileMap.draw(canvas);
        }
    }

    @Override
    public void enter() {
        super.enter();

        this.sound = MediaPlayer.create(UiBridge.getContext(), R.raw.stage02_bgm);
        sound.start();
        sound.setLooping(true);
        Random random = new Random();
        RandomTime = random.nextFloat();
        SpawnTime = 0.f;
        GameData.GameResult = false;

        Initialize();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        // BackGround
        MainBackground BackGround = new MainBackground(cx, cy, width, height, R.mipmap.stage_1);
        gameWorld.add(Layer.bg.ordinal(), BackGround);

        // BackGround
        gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, height, (int) (width * 1.2f), (int) (height * 0.1f), R.mipmap.menubox_2));

        // State UI
        StageUI StageUiInfo = new StageUI(20, 1000, 2);
        gameWorld.add(Layer.ui.ordinal(), StageUiInfo);

        // Skill UI
        SkillUI skillui = new SkillUI(UiBridge.x(50), height - UiBridge.y(25), 0, 0, R.mipmap.meteor);
        gameWorld.add(Layer.ui.ordinal(), skillui);

        // Battle UI
        BattleUI = new StartBattleUI(width - UiBridge.x(100), height - UiBridge.y(25), 0, 0);
        gameWorld.add(Layer.ui.ordinal(), BattleUI);

//        // DecoFlag
//        gameWorld.add(Layer.bg.ordinal(), new DecoFlag(cx - UiBridge.x(130), UiBridge.y(45), 0, 0, R.mipmap.flag0, 9, 9));
//        gameWorld.add(Layer.bg.ordinal(), new DecoFlag(cx + UiBridge.x(130), UiBridge.y(45), 0, 0, R.mipmap.flag0, 9, 9));

        // setting Button
        NinePatchButton btnSettings = new NinePatchButton(width - UiBridge.x(50), UiBridge.y(50), R.mipmap.btn_settings, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btnSettings.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                // SettingUI 나오게 하기
                AddUiScene(new SettingUI());
            }
        });
        gameWorld.add(Layer.ui.ordinal(), btnSettings);

        // tipMark
        TipMark tipMark = new TipMark(width - UiBridge.x(110), UiBridge.y(50), 0, 0, R.mipmap.tipmark);
        tipMark.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                AddUiScene(new TipUI());
            }
        });
        gameWorld.add(Layer.object.ordinal(), tipMark);

        // tile load
        int tileSizeX = width / 25;
        int tileSizeY = height / 19;
        TileMap = TiledMap.load(UiBridge.getResources(), R.raw.stage2, false);
        TileMap.loadImage(UiBridge.getResources(), new int[]{R.mipmap.tilechip}, tileSizeX, tileSizeY);
        LoadTileObject();
    }

    private void LoadTileObject() {
//        // 1 : 초록(이동가능)  2 : 빨강(이동불가)  3 : 검정(깃발)
        for (int i = 0; i < TileMap.height; ++i) {
            for (int j = 0; j < TileMap.width; ++j) {
                // x,y => 해당 타일의 중점 좌표
                int tileX = (j * TileMap.dstTileWith) + (TileMap.dstTileWith / 2);
                int tileY = (i * TileMap.dstTileHeight) + (TileMap.dstTileHeight / 2);
                int tileIndex = (i * TileMap.width) + j; // 0 1 2 3 ...

                if (TileMap.getTileData(tileIndex) == 3) {
                    // 깃발 생성
                    gameWorld.add(Layer.object.ordinal(), new TowerFlag(tileX, tileY, 0, 0, this));
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        // 뒤로가는 UI 생성
        if (GameScene.UiScene != null)
            DeleteUiScene();
        else
            new GameBackUI().push();
    }

    @Override
    public void pause() {
        sound.pause();
    }

    @Override
    public void resume() {
        sound.start();
    }

    @Override
    public void exit() {
        super.exit();
        sound.stop();
        GameData.StageGold = 0;
        GameData.StageLife = 0;
        GameData.StageCurWave = 0;
        GameData.StageMaxWave = 0;
        GameData.GameResult = false;
    }
}
