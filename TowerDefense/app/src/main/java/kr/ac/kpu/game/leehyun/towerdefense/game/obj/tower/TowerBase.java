package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TowerFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

public class TowerBase extends GameObject {
    private static final String TAG = TowerBase.class.getSimpleName();

    private BitmapObject TowerImage;
    private TowerProgress TowerProgressBar;
    private TowerLayer TowerState;

    public enum TowerLayer {
        Archer, Cannon, Magic, Sword, COUNT
    }

    public TowerBase(float x, float y, TowerLayer eState) {
        this.x = x;
        this.y = y ;
        int TowerResId = 0;
        TowerState = eState;

        switch (TowerState) {
            case Archer:
                TowerResId = R.mipmap.archer_begin;
                break;
            case Cannon:
                TowerResId = R.mipmap.cannon_begin;
                break;
            case Magic:
                TowerResId = R.mipmap.magic_begin;
                break;
            case Sword:
                TowerResId = R.mipmap.sword_begin;
                break;
            case COUNT:
                break;
        }
        // 사용할 타워 이미지
        TowerImage = new BitmapObject(x,y,0,0, TowerResId);

        // 타워 게이지바
        TowerProgressBar = TowerProgress.get(x, TowerImage.getY() - TowerImage.getHeight()/2 - UiBridge.y(10));

        GameScene Scene = GameScene.getTop();
        if(Scene instanceof FirstStage) {
            Scene.getGameWorld().add(FirstStage.Layer.object.ordinal(), TowerImage);
            Scene.getGameWorld().add(FirstStage.Layer.object.ordinal(), TowerProgressBar);
        } else if(Scene instanceof SecondStage) {
            Scene.getGameWorld().add(SecondStage.Layer.object.ordinal(), TowerImage);
            Scene.getGameWorld().add(SecondStage.Layer.object.ordinal(), TowerProgressBar);
        }


//        else if (Scene instanceof FirstStage) {
//
//        }

    }

    @Override
    public void update() {
        // TowerProgressBar Update하기
        if(TowerProgressBar.updateRect()) {
            // 타워 State에 따라서 알맞는 타워 생성 하기
            CreateTower();
            TowerProgressBar.remove();
            TowerImage.remove();
            this.remove();
        }
    }

    private void CreateTower() {
        GameScene Scene = GameScene.getTop();
        float BaseY = y - UiBridge.y(10);
        switch (TowerState) {
            case Archer:
                if (Scene instanceof FirstStage) {
                    Scene.getGameWorld().add(FirstStage.Layer.towerobject.ordinal(),
                            Tower_Archer.get(x, BaseY));
                } else if(Scene instanceof SecondStage) {
                    Scene.getGameWorld().add(SecondStage.Layer.towerobject.ordinal(),
                            Tower_Archer.get(x, BaseY));
                }
                break;
            case Cannon:
                if (Scene instanceof FirstStage) {
                    Scene.getGameWorld().add(FirstStage.Layer.towerobject.ordinal(),
                            Tower_Cannon.get(x, BaseY));
                } else if(Scene instanceof SecondStage) {
                    Scene.getGameWorld().add(SecondStage.Layer.towerobject.ordinal(),
                            Tower_Cannon.get(x, BaseY));
                }
                break;
            case Magic:
                if (Scene instanceof FirstStage) {
                    Scene.getGameWorld().add(FirstStage.Layer.towerobject.ordinal(),
                            Tower_Magic.get(x, BaseY));
                } else if(Scene instanceof SecondStage) {
                    Scene.getGameWorld().add(SecondStage.Layer.towerobject.ordinal(),
                            Tower_Magic.get(x, BaseY));
                }
                break;
            case Sword:
                if (Scene instanceof FirstStage) {
                    Scene.getGameWorld().add(FirstStage.Layer.towerobject.ordinal(),
                            Tower_Sword.get(x, BaseY));
                }  else if(Scene instanceof SecondStage) {
                    Scene.getGameWorld().add(SecondStage.Layer.towerobject.ordinal(),
                            Tower_Sword.get(x, BaseY));
                }
                break;
            case COUNT:
                break;
        }
    }
}

