package kr.ac.kpu.game.leehyun.towerdefense.game.data.manager;

import android.graphics.PointF;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.BoostMan;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Wolf;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;

// 몬스터를 생성하는 매니저
public class SpawnManager {
    private static final String TAG = SpawnManager.class.getSimpleName();
    private static SpawnManager singleton;

    private boolean IsBattle;



    public static SpawnManager get() {
        if(singleton == null) {
            singleton = new SpawnManager();
        }
        return singleton;

    }

    public SpawnManager() {
        IsBattle = false;
    }

    public void SetBattle(boolean isbattle) { IsBattle = isbattle;}
    public boolean GetBattle() { return IsBattle; }

    // Monster를 인자로 받고, 인자로 받은 몬스터를 Array에 저장해서 한번에 관리. << 굳이?? 하는 이유는?
    // 각 몬스터는 get으로 생성
    public void MonsterSpawn(GameScene CurrentScene) {
        if(CurrentScene == null)
            return;

        // 1. 몬스터가 이동할 라인을 지정한다 => 라인매니저에서 라인을 받아와야함 [ 몬스터 안에서 처리 함 ]
        // 2. 받은 라인의 첫번째 x,y가 몬스터의 스폰 위치
        // 3. 라인, x,y , 이미지로 몬스터.get() 등록

        LinkedList<PointF> Linelist = LineManager.get().GetRandomLine();
        float x = Linelist.get(0).x;
        float y = Linelist.get(0).y;


       if(CurrentScene instanceof FirstStage) {
           // 0. GameData의 CurWave에 따른 몬스터 분기
           Random random = new Random();
           int iRandom = random.nextInt(2);
           if(iRandom == 0) {        // 부스트맨
               BoostMan boostMan;
               boostMan = BoostMan.get(x,y,Linelist);
               CurrentScene.getGameWorld().add(FirstStage.Layer.enemy.ordinal(), boostMan);
           }

           if(iRandom == 1) {        // 울프
               Wolf wolf;
               wolf = Wolf.get(x,y,Linelist);
               CurrentScene.getGameWorld().add(FirstStage.Layer.enemy.ordinal(), wolf);
           }
        }
    }



}
