package kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui;

import android.graphics.RectF;
import android.text.method.Touch;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;

public class Button extends BitmapObject implements Touchable, BoxCollidable {
    private static final String TAG = Button.class.getSimpleName();
    private Runnable onClickRunnable;
    private RectF CollisionBox;
    private int normalImage = -1;
    private int touchImage = -1;
    private int pressImage =- 1;

    public enum Layer {
        normal, touch, press, COUNT
    }
    public Layer ButtonState = Layer.normal;
    public Button(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
        normalImage = resId;

        CollisionBox = new RectF();
        getBox(CollisionBox);
    }

    public Button(float x, float y, int width, int height, int normalID, int touchID, int pressID) {
        super(x, y, width, height, normalID);
        CollisionBox = new RectF();
        getBox(CollisionBox);
        AddBitmap(touchID);
        AddBitmap(pressID);

        normalImage = normalID;
        touchImage = touchID;
        pressImage = pressID;
    }

    @Override
    public void move(float dx, float dy) {
        super.move(dx, dy);
        getBox(CollisionBox);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    captureTouch();
                    ButtonState = Layer.press;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY()) ) {
                    if(ButtonState != Layer.press)
                        ButtonState = Layer.touch;
                    return true;
                } else {
                    ButtonState = Layer.normal;
                }
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    if (onClickRunnable != null) {
                        onClickRunnable.run();
                    }
                    ButtonState = Layer.normal;
                    return true;
                }
                break;

        }
        return false;
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    @Override
    public void update() {
        switch (ButtonState) {
            case normal:
                if(normalImage != -1)
                    LoadBitmap(normalImage);
                break;

            case touch:
                if(touchImage != -1)
                    LoadBitmap(touchImage);
                else
                    LoadBitmap(normalImage);
                break;

            case press:
                if(pressImage != -1)
                    LoadBitmap(pressImage);
                else
                    LoadBitmap(normalImage);
                break;

            case COUNT:
                break;
        }
    }

    // 클릭시 발생할 이벤트
    public void setOnClickRunnable(Runnable runnable) {
        this.onClickRunnable = runnable;
    }
}
