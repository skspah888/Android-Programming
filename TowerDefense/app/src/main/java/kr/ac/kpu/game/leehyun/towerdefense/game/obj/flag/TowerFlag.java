package kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag;

import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.TowerSelect;

public class TowerFlag extends BitmapObject implements Touchable {
    private static final String TAG = TowerFlag.class.getSimpleName();
    private RectF CollisionBox;
    private GameScene CurrentScene;

    public TowerFlag(float x, float y, int width, int height, GameScene Scene) {
        super(x, y, width, height, R.mipmap.flag);
        CurrentScene = Scene;
        CollisionBox = new RectF();
        getBox(CollisionBox);
    }

    @Override
    public void update() {
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    captureTouch();
                    // UI 생성
                    if(GameScene.getUiScene() == null) {
                        CurrentScene.AddUiScene(new TowerSelect(x,y, this));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }
}
