package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;

public class HPBar extends GameObject implements Recyclable {
    private static final String TAG = HPBar.class.getSimpleName();

    private SharedBitmap FrontProgress;
    private SharedBitmap BackProgress;

    private RectF DrawRect = new RectF();
    private Rect ProgressRect = new Rect();
    private Monster MyMonster;
    private float MAXHP;

    private float MaxValue;

    public HPBar(float x, float y, Monster monster) {
        this.x = x;     // 중앙
        this.y = y;

        MyMonster = monster;
        MAXHP = MyMonster.GetHp();

        // 사용할 타워 이미지
        FrontProgress = SharedBitmap.load(R.mipmap.monsterhp_front, false);
        BackProgress = SharedBitmap.load(R.mipmap.monsterhp_back, false);

        ProgressRect.top = (int) (y - FrontProgress.getHeight()/2);
        ProgressRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        ProgressRect.left = (int) (x - FrontProgress.getWidth()/2);
        ProgressRect.right = (int) (x + FrontProgress.getWidth()/2);

        DrawRect.top = (int) (y - FrontProgress.getHeight()/2);
        DrawRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        DrawRect.left = (int) (x - FrontProgress.getWidth()/2);
        DrawRect.right = (int) (x - FrontProgress.getWidth()/2);

        MaxValue = (x + FrontProgress.getWidth() * 0.5f);
    }


    public static HPBar get(float x, float y, Monster monster) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        HPBar item = (HPBar) rpool.get(HPBar.class);
        if (item == null) {
            item = new HPBar(x, y, monster);
        } else {
            item.Intialize(x,y,monster);
        }
        return item;
    }

    public void Intialize(float x, float y,Monster monster) {
        this.x = x;     // 중앙
        this.y = y;
        this.MyMonster = monster;
        MAXHP = this.MyMonster.GetHp();

        ProgressRect.top = (int) (y - FrontProgress.getHeight()/2);
        ProgressRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        ProgressRect.left = (int) (x - FrontProgress.getWidth()/2);
        ProgressRect.right = (int) (x + FrontProgress.getWidth()/2);

        DrawRect.top = (int) (y - FrontProgress.getHeight()/2);
        DrawRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        DrawRect.left = (int) (x - FrontProgress.getWidth()/2);
        DrawRect.right = (int) (x + FrontProgress.getWidth()/2);
        MaxValue= (int) (x + FrontProgress.getWidth()/2);
    }

    @Override
    public void update() {
        updateRect();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(BackProgress.getBitmap(), ProgressRect, ProgressRect, null);
        canvas.drawBitmap(FrontProgress.getBitmap(), ProgressRect, DrawRect, null);
    }

    public void updateRect() {
        x = MyMonster.getX();
        y = (float) (MyMonster.getY() - MyMonster.GetHeight() * 1.5f);

        ProgressRect.top = (int) (y - FrontProgress.getHeight()/2);
        ProgressRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        ProgressRect.left = (int) (x - FrontProgress.getWidth()/2);
        ProgressRect.right = (int) (x + FrontProgress.getWidth()/2);

        DrawRect.top = (int) (y - FrontProgress.getHeight()/2);
        DrawRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        DrawRect.left = (int) (x - FrontProgress.getWidth()/2);

        DrawRect.right =  DrawRect.left +  (ProgressRect.right - ProgressRect.left) * MyMonster.GetHp() / MAXHP;
    }

    @Override
    public void recycle() {
    }
}

// 지우는거 : remove()