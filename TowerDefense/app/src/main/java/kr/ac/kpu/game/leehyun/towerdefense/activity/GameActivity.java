package kr.ac.kpu.game.leehyun.towerdefense.activity;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import kr.ac.kpu.game.leehyun.towerdefense.framework.input.sensor.GyroSensor;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.view.GameView;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.MainMenu;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SeletMap;

public class GameActivity extends AppCompatActivity {

    private static final long BACKKEY_INTERVAL_MSEC = 1000;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UiBridge.setActivity(this);
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(new GameView(this));

        SoundEffects se = SoundEffects.get();
        se.loadAll(UiBridge.getContext());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        switch (GameData.TestStage)
        {
            case MainMenu:
                new MainMenu().run();
                break;
            case SeletMap:
                new SeletMap().run();
                break;
            case FirstStage:
                new FirstStage().run();
            case SecondStage:
                new SecondStage().run();
                break;
            case COUNT:
                break;
        }
    }

    private long lastBackPressedOn;
    @Override
    public void onBackPressed() {
        long now = System.currentTimeMillis();
        long elapsed = now - lastBackPressedOn;
        if (elapsed <= BACKKEY_INTERVAL_MSEC) {
            handleBackPressed();
            return;
        }
        Log.d("BackKey", "elapsed="+elapsed);
        Toast.makeText(this,
                "Press Back key twice quickly to exit",
                Toast.LENGTH_SHORT)
                .show();
        lastBackPressedOn = now;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GyroSensor.isCreated()) {
            GyroSensor.get().register();
        }
    }

    @Override
    protected void onPause() {
        if (GyroSensor.isCreated()) {
            GyroSensor.get().unregister();
        }
        super.onPause();
    }

    public void handleBackPressed() {
        GameScene.getTop().onBackPressed();
    }

    public void ButtonSpawnSolution()
    {
        // 스코어 오브젝트 생성 방법
//        RectF rbox = new RectF(UiBridge.x(-52), UiBridge.y(20), UiBridge.x(-20), UiBridge.y(62));
//        scoreObject = new ScoreObject(R.mipmap.number_64x84, rbox);
//        gameWorld.add(SecondScene.Layer.ui.ordinal(), scoreObject);

        // 라이프 오브젝트 생성 방법
//        RectF lbox = new RectF(UiBridge.x(20), UiBridge.y(20), UiBridge.x(44), UiBridge.y(42));
//        lifeObject = new LifeObject(R.mipmap.hearts, lbox, LifeObject.COOKIE_LIFE);
//        gameWorld.add(Layer.ui.ordinal(), lifeObject);
    }

    // 게임 재시작 방법 : Scene에서 사용
    public void Restart() {
//        // 1. Scene이 담당하는 오브젝트 모두 reset
//        lifeObject.reset();
//        scoreObject.reset();
//
//        // 2. gameWorld의 Layer에 있는 모든 오브젝트 삭제 및 Pooling
//        for (int layer = Layer.platform.ordinal(); layer <= Layer.obstacle.ordinal(); layer++) {
//            gameWorld.removeAllObjectsAt(layer);
//        }
//        map.reset();
    }
}
