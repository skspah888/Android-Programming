package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.framework.util.CollisionHelper;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc.CannonBomb;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.TowerUpgrade;

public class Tower_Cannon extends Tower {

    private Monster TargetMonster = null;
    private RectF DetectBox = new RectF();

    public Tower_Cannon(float x, float y) {
        super(x, y, 0, 0, R.mipmap.cannon_level1, 18, 18);

        FAB_ONE = new FrameAnimationBitmap(R.mipmap.cannon_level1, 18, 18);
        FAB_ONE.SetFrameSpeed(0);
        FAB_TWO = new FrameAnimationBitmap(R.mipmap.cannon_level2, 20, 20);
        FAB_TWO.SetFrameSpeed(0);
        FAB_THREE = new FrameAnimationBitmap(R.mipmap.cannon_level3, 17, 17);
        FAB_THREE.SetFrameSpeed(0);
        FAB_FOUR = new FrameAnimationBitmap(R.mipmap.cannon_level4, 37, 37);
        FAB_FOUR.SetFrameSpeed(0);

        SetTowerState(1);
        getBox(CollisionBox);
        getDetectBox(DetectBox);
    }

    public static Tower_Cannon get(float x, float y) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Tower_Cannon item = (Tower_Cannon) rpool.get(Tower_Cannon.class);
        if (item == null) {
            item = new Tower_Cannon(x, y);
        } else {
            item.Initialize(x, y);
        }
        return item;
    }

    public void getDetectBox(RectF rect) {
        float hw = fab.getWidth() * 8.f;
        float hh = fab.getHeight() * 8.f;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }


    @Override
    public void Initialize(float x, float y) {
        this.x = x;
        this.y = y;
        SetTowerState(1);
        getBox(this.CollisionBox);
        NextState = BehaivorState.idle;
        fab.SetFrameSpeed(0);

        getDetectBox(DetectBox);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    captureTouch();
                    if(GameScene.getUiScene() == null && GameScene.getTop() != null) {
                        GameScene.getTop().AddUiScene(new TowerUpgrade(x,y,this));
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void SetTowerState(int Level) {
        TowerLevel = Level;

        switch (TowerLevel) {
            case 1:
                fab = FAB_ONE;
                TowerAttack = 10;
                TowerGold = 100;
                TowerSpeed = 100;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.cannontower_01, 0);
                break;
            case 2:
                fab = FAB_TWO;
                TowerAttack = 20;
                TowerGold = 200;
                TowerSpeed = 150;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.cannontower_02, 0);
                break;
            case 3:
                fab = FAB_THREE;
                TowerAttack = 30;
                TowerGold = 300;
                TowerSpeed = 200;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.cannontower_03, 0);
                break;
            case 4:
                fab = FAB_FOUR;
                TowerAttack = 40;
                TowerGold = 400;
                TowerSpeed = 250;
                CurState = BehaivorState.COUNT;
                NextState = BehaivorState.idle;
                SoundEffects.get().play(R.raw.cannontower_04, 0);
                break;
            default:
                break;
        }

        CurState = BehaivorState.COUNT;
        NextState = BehaivorState.idle;
    }

    @Override
    public void update() {
        CheckState();
        UpdateState();
    }

    @Override
    public void UpdateState() {
        switch (CurState) {
            case idle:
                UpdateIdle();
                break;
            case attack:
                UpdateAttack();
                break;
            case COUNT:
                break;
        }
    }

    @Override
    public void CheckState() {
        if(CurState == NextState)
            return;

        switch (NextState) {
            case idle:
                fab.SetFrameSpeed(0);
                break;
            case attack:
                fab.ResetFrameSpeed();
                break;
            case COUNT:
                break;
        }
        CurState = NextState;
    }

    @Override
    public void UpdateIdle() {
        // 근처 적 찾기
        SearchMonster();

        // 적을 찾았으면 Attack 상태로
        if(TargetMonster != null) {
            NextState = BehaivorState.attack;
            fab.ResetFrameSpeed();
        }
    }

    @Override
    public void UpdateAttack() {
        // 찾은 적이 없으면 State를 Idle 상태로
        SearchMonster();

        if(TargetMonster == null) {
            NextState =  BehaivorState.idle;
            fab.SetFrameSpeed(0);
            return;
        }
        else {
            // 적 위치에 따라 방향 변경
            if(fab.done()) {
                // 폭탄 발사
                GameScene Scene = GameScene.getTop();
                if(Scene != null) {
                    if(Scene instanceof FirstStage) {
                        Scene.getGameWorld().add(FirstStage.Layer.object.ordinal(), new CannonBomb(x,y - UiBridge.y(20), TowerLevel, TowerAttack, TowerSpeed, TargetMonster));
                        NextState = BehaivorState.idle;
                        TargetMonster = null;
                        SoundEffects.get().play(R.raw.cannontower_bomb, 0);
                    }
                }
            }
        }

    }

    @Override
    public boolean UpgradeTower() {
        // 플레이어 돈 차감
        if(super.UpgradeTower()) {
            TowerLevel += 1;
            if (TowerLevel > TowerMaxLevel) {
                TowerLevel = TowerMaxLevel;
                return false;
            }
            SetTowerState(TowerLevel);
        }

        return true;
    }

    @Override
    public void recycle() {

    }

    private void SearchMonster() {
            GameScene Scene = GameScene.getTop();
            if(Scene == null)
                return;

            if(Scene instanceof FirstStage) {
                ArrayList<GameObject> enemyList = Scene.getGameWorld().objectsAtLayer(FirstStage.Layer.enemy.ordinal());
                for (GameObject obj : enemyList) {
                    if (!(obj instanceof Monster)) {
                        continue;
                    }
                    Monster monster = (Monster) obj;
                    if (CollisionHelper.collides(DetectBox, monster)) {
                        if(TargetMonster == null) {
                            TargetMonster = monster;
                            return;
                        }  else {
                            // 거리 비교해서 더 가까운 몬스터 등록
                            float targetX = TargetMonster.getX() - this.x;
                            float targetY = TargetMonster.getY() - this.y;
                            float targetDist = (float) Math.sqrt((targetX * targetX + targetY * targetY));

                            float dx = monster.getX() - this.x;
                            float dy = monster.getY() - this.y;
                            float fDist = (float) Math.sqrt((dx * dx + dy * dy));

                            if(fDist < targetDist) {
                                TargetMonster = monster;
                                return;
                            }
                        }
                    }
                }
            }
    }
}
