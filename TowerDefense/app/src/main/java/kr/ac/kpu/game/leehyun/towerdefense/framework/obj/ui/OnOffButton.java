package kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui;

import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;

public class OnOffButton extends BitmapObject implements Touchable, BoxCollidable {
    private static final String TAG = OnOffButton.class.getSimpleName();
    private Runnable onClickRunnable;
    private RectF CollisionBox;
    private int offImage = -1;
    private int onImage = -1;

    public enum Layer {
        off, on, COUNT
    }

    public enum Type {
        tile, stage, COUNT
    }

    public Layer ButtonState = Layer.COUNT;
    public Type ButtonType = Type.COUNT;

    public OnOffButton(float x, float y, int width, int height, int offId, int onId, Type eType) {
        super(x, y, width, height, offId);
        CollisionBox = new RectF();
        getBox(CollisionBox);
        AddBitmap(onId);

        onImage = onId;
        offImage = offId;
        ButtonType = eType;
        setButtonState();

    }

    @Override
    public void move(float dx, float dy) {
        super.move(dx, dy);
        getBox(CollisionBox);
    }

    private void setButtonState() {
        // GameData 를 토대로 on/off 결정하기
        switch (ButtonType) {
            case tile:
                if(GameData.DrawTile)
                    ButtonState = Layer.on;
                else
                    ButtonState = Layer.off;
                break;

            case stage:
                if(GameData.Stage02_Open)
                    ButtonState = Layer.on;
                else
                    ButtonState = Layer.off;
            case COUNT:
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    captureTouch();
                    ButtonState = Layer.off;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int)e.getX(), (int)e.getY())) {
                    if (onClickRunnable != null) {
                        onClickRunnable.run();
                    }
                    setButtonState();
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    @Override
    public void update() {
        switch (ButtonState) {
            case on:
                if(onImage != -1)
                    LoadBitmap(onImage);
                break;

            case off:
                if(offImage != -1)
                    LoadBitmap(offImage);
                break;
            case COUNT:
                break;
        }
    }

    // 클릭시 발생할 이벤트
    public void setOnClickRunnable(Runnable runnable) {
        this.onClickRunnable = runnable;
    }
}
