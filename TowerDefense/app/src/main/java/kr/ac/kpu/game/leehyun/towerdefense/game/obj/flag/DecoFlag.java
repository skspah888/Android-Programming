package kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag;

import android.graphics.Canvas;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.StageExplanation;

public class DecoFlag extends Flag  {
    public DecoFlag(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, -80, resId, fps, count);
    }
}
