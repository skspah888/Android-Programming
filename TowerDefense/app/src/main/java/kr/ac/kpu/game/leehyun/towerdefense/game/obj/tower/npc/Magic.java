package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower.npc;

import android.graphics.PointF;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.manager.EffectManager;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.Effects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.monster.Monster;

public class Magic extends AnimObject implements Recyclable {

    private PointF Direction = new PointF();

    private FrameAnimationBitmap L1_Magic;
    private FrameAnimationBitmap L2_Magic;
    private FrameAnimationBitmap L3_Magic;
    private FrameAnimationBitmap L4_Magic;

    private Monster TargetMonster;
    private PointF GoalPoint = new PointF();
    private int MagicLevel = 0;
    private int MagicAttack = 0;
    private int MagicSpeed = 0;
    private int iEvent = 0;

    public Magic(float x, float y, int level, int attack, int speed, Monster monster) {
        super(x,y,0,0, R.mipmap.magic_bullet1, 3,3);

        L1_Magic = new FrameAnimationBitmap(R.mipmap.magic_bullet1,3,3);
        L2_Magic = new FrameAnimationBitmap(R.mipmap.magic_bullet2,3,3);
        L3_Magic = new FrameAnimationBitmap(R.mipmap.magic_bullet3,3,3);
        L4_Magic = new FrameAnimationBitmap(R.mipmap.magic_bullet4,12,12);

        Initialize(x, y, level, attack, speed, monster);
    }

    public static Magic get(float x, float y, int level, int attack, int speed, Monster monster) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        Magic item = (Magic) rpool.get(Magic.class);
        if (item == null) {
            item = new Magic(x, y, level, attack, speed, monster);
        } else {
            item.Initialize(x, y, level, attack, speed, monster);
        }
        return item;
    }

    private void Initialize(float x, float y, int level, int attack, int speed, Monster monster) {
        MagicLevel = level;
        MagicAttack = attack;
        MagicSpeed = speed;
        TargetMonster = monster;
        GoalPoint.x = TargetMonster.getX();
        GoalPoint.y = TargetMonster.getY();

        switch (MagicLevel) {
            case 1:
                fab = L1_Magic;
                fab.ResetFrameSpeed();
                break;
            case 2:
                fab = L2_Magic;
                fab.ResetFrameSpeed();
                break;
            case 3:
                fab = L3_Magic;
                fab.ResetFrameSpeed();
                break;
            case 4:
                fab = L4_Magic;
                fab.ResetFrameSpeed();
                break;
        }
    }

    @Override
    public void update() {
        if (iEvent == -1) {
            if (TargetMonster != null)
                TargetMonster.DecreaseHp(MagicAttack);
            remove();
            return;
        }

        UpdatePoint();
        UpdateDirection();

        x += Direction.x * MagicSpeed * GameTimer.getTimeDiffSeconds();
        y += Direction.y * MagicSpeed * GameTimer.getTimeDiffSeconds();
    }

    private void UpdatePoint() {
        if (TargetMonster != null) {
            GoalPoint.x = TargetMonster.getX();
            GoalPoint.y = TargetMonster.getY();
        }
    }

    private void UpdateDirection() {
        // 목적지를 기반으로 Direction 설정
        PointF CurPos = new PointF(x, y);
        Direction.x = (GoalPoint.x - CurPos.x);
        Direction.y = (GoalPoint.y - CurPos.y);
        float Length = Direction.length();
        Direction.x = Direction.x / Length;
        Direction.y = Direction.y / Length;

        float dx = GoalPoint.x - this.x;
        float dy = GoalPoint.y - this.y;
        float fDist = (float) Math.sqrt((dx * dx + dy * dy));

        if(fDist < 10.f) {
            iEvent = -1;
        }
    }

    @Override
    public void recycle() {

    }
}
