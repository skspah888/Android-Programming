package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.Button;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;

public class GameBackUI extends GameScene {
    private static final String TAG = GameBackUI.class.getSimpleName();
    private final Paint paint;
    private String ErrorText;
    private Button BackButton;
    private Button MenuButton;

    public GameBackUI() {
        ErrorText = "메뉴로 돌아가시겠습니까??";
        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(70.f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(Color.WHITE);
    }

    public enum Layer {
        bg , ui  , COUNT
    }

    @Override
    protected int getLayerCount() {
        return GameBackUI.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        Initialize();

//        // 애니메이션 추가
//        int mdpi_100 = UiBridge.y(100);
//        //ValueAnimator animator =
//        ValueAnimator anim = ValueAnimator.ofFloat(UiBridge.metrics.size.y, mdpi_100);
//        anim.setDuration(500);
//        anim.setInterpolator(new OvershootInterpolator());
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                Float value = (Float) animation.getAnimatedValue();
//                scrollTo(value);
//            }
//        });
//        anim.start();
    }

    private void scrollTo(float y) {
        int mdpi_100 = UiBridge.y(100);
        ArrayList<GameObject> objs = gameWorld.objectsAtLayer(Layer.ui.ordinal());
        int count = objs.size();
        for (int i = 0; i < count; i++) {
            Button btn = (Button)objs.get(i);
            float diff = y - btn.getY();
            btn.move(0, diff);

            y += mdpi_100;
        }
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        float UiWidth = (float)width;
        float UiHeight = (float)height * 0.6f;

        // Menu Box
        gameWorld.add(Layer.ui.ordinal(), new MainBackground(cx, cy, (int)UiWidth, (int)UiHeight, R.mipmap.menubox_5));

        // Menu 버튼
        MenuButton =  new Button(cx - UiBridge.x(100), cy + UiBridge.y(50), -120, -120,
                R.mipmap.menu_touch, R.mipmap.menu_touch, R.mipmap.menu_press);
        MenuButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                GameScene.getTop().onBackPressed();
                AddUiScene(new WindowChange(WindowChange.SceneLayer.Select, WindowChange.StackLayer.Back));
            }
        });
        gameWorld.add(Layer.ui.ordinal(), MenuButton);


        // Back 버튼
        BackButton = new Button(cx + UiBridge.x(100), cy + UiBridge.y(50), -120, -120,
                R.mipmap.back2_touch, R.mipmap.back2_touch, R.mipmap.back2_press);
        BackButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                GameScene.getTop().onBackPressed();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), BackButton);

        // Black UI
        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(cx, cy, width, height, R.mipmap.black_alpha));

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        super.draw(canvas);
        canvas.drawText(ErrorText, UiBridge.metrics.center.x, UiBridge.metrics.center.y - UiBridge.y(25), paint);
        canvas.restore();
    }
}
