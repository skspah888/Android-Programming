package kr.ac.kpu.game.leehyun.towerdefense.framework.iface;

import android.graphics.RectF;

public interface BoxCollidable {
    public void getBox(RectF rect);
}
