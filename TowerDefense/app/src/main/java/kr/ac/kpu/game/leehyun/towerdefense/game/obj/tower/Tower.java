package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;

public abstract class Tower extends AnimObject implements Touchable, BoxCollidable, Recyclable {
    protected static final String TAG = Tower.class.getSimpleName();
    protected FrameAnimationBitmap FAB_ONE;
    protected FrameAnimationBitmap FAB_TWO;
    protected FrameAnimationBitmap FAB_THREE;
    protected FrameAnimationBitmap FAB_FOUR;
    protected TowerBase.TowerLayer TowerState;
    protected BehaivorState CurState = BehaivorState.COUNT;
    protected BehaivorState NextState = BehaivorState.COUNT;

    protected RectF CollisionBox;
    protected int TowerLevel = 0;
    protected int TowerAttack = 0;
    protected int TowerGold = 0;
    protected int TowerSpeed = 0;
    protected static int TowerMaxLevel = 4;

    public enum BehaivorState {
        idle, attack, COUNT
    }

    public int GetLevel() { return TowerLevel;}
    public int GetAttack() { return TowerAttack;}
    public int GetGold() { return TowerGold;}

    public Tower(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, height, resId, fps, count);
        fab.SetFrameSpeed(0);
        CollisionBox = new RectF();
        NextState = BehaivorState.idle;
    }

    public abstract void Initialize(float x, float y);

    @Override
    public abstract boolean onTouchEvent(MotionEvent e);

    public abstract void SetTowerState(int Level);

    public abstract void UpdateState();
    public abstract void CheckState();
    public abstract void UpdateIdle();
    public abstract void UpdateAttack();

    public boolean UpgradeTower() {
        if(GameData.StageGold - TowerGold < 0)
            return false;
        GameData.StageGold -= TowerGold;
        return true;
    }

    public void RemoveTower() {
        GameData.StageGold += TowerGold;
        if(GameData.StageGold < 0)
            GameData.StageGold = 0;
        remove();
    }

    public void SetTowerBehavior(BehaivorState eState) {
        NextState = eState;
    }

    @Override
    public void getBox(RectF rect) {
        int hw = (int)fab.getWidth() / 2;
        int hh = (int)fab.getHeight() / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
