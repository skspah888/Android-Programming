package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.manager.SpawnManager;

public class StartBattleUI extends BitmapObject implements Touchable {
    private static final String TAG = StartBattleUI.class.getSimpleName();
    private final Paint paint;
    private String TimeText;
    private float WaveMaxTime = 10f;
    private float WaveCurTime = 10f;
    private BitmapObject Battle_Off;
    private BitmapObject Battle_On;

    private RectF CollisionBox;

    private boolean IsBattle;

    public StartBattleUI(float x, float y, int width, int height) {
        super(x, y, width, height, R.mipmap.menubox_2);

        int size = -80;
        Battle_Off = new BitmapObject(x + UiBridge.x(30),y,size,size,R.mipmap.battle_off);
        Battle_On = new BitmapObject(x + UiBridge.x(30),y,size,size,R.mipmap.battle_on);

        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(80.f);
        paint.setColor(Color.WHITE);

        TimeText = Integer.toString((int)WaveMaxTime);
        IsBattle = false;

        CollisionBox = new RectF();
        CollisionBox.top = (int) (y - Battle_Off.getHeight() * 0.4f);
        CollisionBox.bottom = (int) (y + Battle_Off.getHeight()* 0.4f);
        CollisionBox.left = (int) (x - Battle_Off.getWidth()* 0.4f);
        CollisionBox.right = (int) (x + Battle_Off.getWidth()* 0.4f);
    }

    @Override
    public void update() {
        MakeText();
        UpdateState();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        // 뒤 배경
        super.draw(canvas);

        // 배틀버튼
        if(IsBattle) {
            Battle_On.draw(canvas);
        } else {
            Battle_Off.draw(canvas);
        }

        // 텍스트
        canvas.drawText(TimeText, x - UiBridge.x(45), y + UiBridge.y(10), paint);
        canvas.restore();
    }

    public void MakeText() {
        if(!IsBattle)
            return;

        WaveCurTime -= GameTimer.getTimeDiffSeconds();
        TimeText = Integer.toString((int)WaveCurTime);
    }

    public void UpdateState() {
        // 시간이 0초가 되면 Battle을 클릭 가능한 상태로 변경
        if(WaveCurTime < 0.f) {
            WaveCurTime = WaveMaxTime;
            TimeText = Integer.toString((int)WaveCurTime);
            IsBattle = false;
            SpawnManager.get().SetBattle(IsBattle);
        }

        // 몬스터가 다 죽으면 Battle을 클릭가능한 상태로 변경

    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    if(!IsBattle && (GameData.StageCurWave < GameData.StageMaxWave)) {
                        IsBattle = true;
                        SoundEffects.get().play(R.raw.startbattle, 0);
                        SpawnManager.get().SetBattle(IsBattle);
                        GameData.StageCurWave += 1;
                    }
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (CollisionBox.contains((int) e.getX(), (int) e.getY())) {

                    return true;
                }
                break;
        }
        return false;
    }
}
