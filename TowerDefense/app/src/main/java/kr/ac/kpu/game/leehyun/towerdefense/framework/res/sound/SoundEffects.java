package kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;

import java.util.HashMap;

import kr.ac.kpu.game.leehyun.towerdefense.R;

public class SoundEffects {
    private static final String TAG = SoundEffects.class.getSimpleName();
    private static SoundEffects singleton;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundIdMap = new HashMap<>();
    private static final int[] SOUND_IDS = {
            R.raw.archerattack_bow, R.raw.archerattack_gun,
            R.raw.archertower_01, R.raw.archertower_02, R.raw.archertower_03, R.raw.archertower_04,
            R.raw.booster_die, R.raw.booster_init, R.raw.booster_move,
            R.raw.button_start, R.raw.cannontower_bomb,
            R.raw.cannontower_01, R.raw.cannontower_02, R.raw.cannontower_03, R.raw.cannontower_04,
            R.raw.click_00, R.raw.click_01, R.raw.freedom, R.raw.magicattack,
            R.raw.magicower_01, R.raw.magictower_02, R.raw.magictower_03, R.raw.magictower_04,
            R.raw.meteor_bomb, R.raw.meteor_click, R.raw.pangapre,
            R.raw.skill_move,
            R.raw.swordtower_01, R.raw.swordtower_02, R.raw.swordtower_03, R.raw.swordtower_04,
            R.raw.towerbuild, R.raw.towerselect,
            R.raw.windowclose, R.raw.windowmove, R.raw.windowopen,
            R.raw.wolf_init, R.raw.wolf_die, R.raw.wolf_move, R.raw.fire, R.raw.startbattle
            , R.raw.monster_die
    };

    public static SoundEffects get() {
        if (singleton == null) {
            singleton = new SoundEffects();
        }
        return singleton;
    }
    private SoundEffects() {
        AudioAttributes audioAttributes;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            this.soundPool = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(3)
                    .build();
        } else {
            this.soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }
    }
    public void loadAll(Context context) {
        for (int resId: SOUND_IDS) {
            int soundId = soundPool.load(context, resId, 1);
            soundIdMap.put(resId, soundId);
        }
    }

    public int play(int resId, int loop) {
        int soundId = soundIdMap.get(resId);
        int streamId = soundPool.play(soundId, 1f, 1f, 1, loop, 1f);
        return streamId;
    }

    public void StopSound(int streamId) {
        soundPool.stop(streamId);
    }
}
