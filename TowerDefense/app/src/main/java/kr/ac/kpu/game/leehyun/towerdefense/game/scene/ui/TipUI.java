package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.Button;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;

public class TipUI extends GameScene {
    private static final String TAG = TipUI.class.getSimpleName();
    private final Paint paint;
    private String[] TextArr = {
            "모든 타워는 4레벨까지 있습니다." ,
            "HP가 0이되면 게임이 종료됩니다." ,
            "몬스터는 경로를 따라 움직입니다." ,
            "타워종류에 따라 공격력이 다릅니다." ,
            "스킬에는 쿨타임이 있습니다." ,
            "업그레이드에는 일정 골드가 필요합니다." ,
            "몬스터를 잡으면 골드를 얻습니다." ,
            "맵을 클리어하면 다음 맵이 해금됩니다" ,
            "꽝!!" ,
            "Made by 2014182037 이현" ,
    };

    private String TipText;
    private Button BackButton;

    public TipUI() {
        Random random = new Random();
        int index = random.nextInt(TextArr.length);
        TipText = TextArr[index];

        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(50.f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(Color.WHITE);
        SoundEffects.get().play(R.raw.click_01, 0);
    }

    public enum Layer {
        bg , button , text , COUNT
    }

    @Override
    protected int getLayerCount() {
        return TipUI.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        Initialize();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        float UiWidth = (float)width * 0.7f;
        float UiHeight = (float)height * 0.5f;

        gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, cy, (int)UiWidth, (int)UiHeight, R.mipmap.tipbox));

        // Back 버튼
        BackButton = new Button(cx , cy + UiBridge.y(60), 0, 0,
                R.mipmap.back_normal, R.mipmap.back_touch, R.mipmap.back_press);
        BackButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                DeleteUiScene();
                SoundEffects.get().play(R.raw.click_00, 0);
            }
        });
        gameWorld.add(Layer.button.ordinal(), BackButton);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        super.draw(canvas);
        canvas.drawText(TipText, UiBridge.metrics.center.x, UiBridge.metrics.center.y - UiBridge.y(10), paint);
        canvas.restore();
    }
}
