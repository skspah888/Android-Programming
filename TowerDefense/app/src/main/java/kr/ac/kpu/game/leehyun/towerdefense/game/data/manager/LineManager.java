package kr.ac.kpu.game.leehyun.towerdefense.game.data.manager;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;

// 몬스터 이동 경로를 가지고 있는 라인 매니저
public class LineManager {
    private static final String TAG = LineManager.class.getSimpleName();
    private static LineManager singleton;

    HashMap<Integer, LinkedList<PointF>> LineMap;

    public static LineManager get() {
        if(singleton == null) {
            singleton = new LineManager();
        }
        return singleton;
    }

    // 생성자, 여기서 라인을 등록한다.
    private LineManager() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;
        LineMap = new HashMap<Integer, LinkedList<PointF>>();

        // 첫번째 라인 [ 왼쪽 출발 ]
        LinkedList<PointF> LineLeft = new LinkedList<PointF>();
        PointF p0 = new PointF(UiBridge.x(0), height - UiBridge.y(95)); LineLeft.add(p0);
        PointF p1 = new PointF(UiBridge.x(185), height - UiBridge.y(103)); LineLeft.add(p1);
        PointF p2 = new PointF(UiBridge.x(185), height - UiBridge.y(185)); LineLeft.add(p2);
        PointF p3 = new PointF(UiBridge.x(100), height - UiBridge.y(205)); LineLeft.add(p3);
        PointF p4 = new PointF(UiBridge.x(118), height - UiBridge.y(268)); LineLeft.add(p4);
        PointF p5 = new PointF(cx - UiBridge.x(20), height - UiBridge.y(273)); LineLeft.add(p5);
        PointF p6 = new PointF(cx- UiBridge.x(20), UiBridge.y(0)); LineLeft.add(p6);
        LineMap.put(0,LineLeft);

        LinkedList<PointF> LineRight = new LinkedList<PointF>();
        PointF p7 = new PointF(width, height - UiBridge.y(95)); LineRight.add(p7);
        PointF p8 = new PointF(width - UiBridge.x(185), height - UiBridge.y(103)); LineRight.add(p8);
        PointF p9 = new PointF(width - UiBridge.x(185), height - UiBridge.y(185)); LineRight.add(p9);
        PointF p10 = new PointF(width - UiBridge.x(100), height - UiBridge.y(205)); LineRight.add(p10);
        PointF p11 = new PointF(width - UiBridge.x(118), height - UiBridge.y(268)); LineRight.add(p11);
        PointF p12 = new PointF(cx + UiBridge.x(20), height - UiBridge.y(273)); LineRight.add(p12);
        PointF p13 = new PointF(cx + UiBridge.x(20), UiBridge.y(0)); LineRight.add(p13);
        LineMap.put(1,LineRight);

        LinkedList<PointF> LineMiddleLeft = new LinkedList<PointF>();
        PointF p14 = new PointF(cx- UiBridge.x(20), height); LineMiddleLeft.add(p14);
        PointF p15 = new PointF(cx- UiBridge.x(17), height - UiBridge.y(98)); LineMiddleLeft.add(p15);
        PointF p16 = new PointF(cx - UiBridge.x(150), height - UiBridge.y(107)); LineMiddleLeft.add(p16);
        PointF p17 = new PointF(cx - UiBridge.x(156), height - UiBridge.y(190)); LineMiddleLeft.add(p17);
        PointF p18 = new PointF(cx - UiBridge.x(10), cy + UiBridge.y(11)); LineMiddleLeft.add(p18);
        PointF p19 = new PointF(cx - UiBridge.x(20), 0); LineMiddleLeft.add(p19);
        LineMap.put(2,LineMiddleLeft);

        LinkedList<PointF> LineMiddleRight = new LinkedList<PointF>();
        PointF p20 = new PointF(cx + UiBridge.x(20), height); LineMiddleRight.add(p20);
        PointF p21 = new PointF(cx + UiBridge.x(17), height - UiBridge.y(98)); LineMiddleRight.add(p21);
        PointF p22 = new PointF(cx + UiBridge.x(150), height - UiBridge.y(107)); LineMiddleRight.add(p22);
        PointF p23 = new PointF(cx + UiBridge.x(156), height - UiBridge.y(190)); LineMiddleRight.add(p23);
        PointF p24 = new PointF(cx + UiBridge.x(10), cy + UiBridge.y(11)); LineMiddleRight.add(p24);
        PointF p25 = new PointF(cx + UiBridge.x(20), 0); LineMiddleRight.add(p25);
        LineMap.put(3,LineMiddleRight);
    }

    public LinkedList<PointF> GetRandomLine() {
        Random random = new Random();
        int randomInt = random.nextInt(LineMap.size());

        return LineMap.get(randomInt);
    }

}
