package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.TowerFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SecondStage;

public class UpgradeTowerUI extends BitmapObject implements Touchable {
    private static final String TAG = UpgradeTowerUI.class.getSimpleName();
    private RectF CollisionBox;
    private GameScene gameScene;
    private boolean IsDead;
    private Tower tower;
    private Paint paint;

    public UpgradeTowerUI(float x, float y, int width, int height, Tower _tower) {
        super(x, y, width, height, R.mipmap.upgrade_ui2);
        tower = _tower;
        CollisionBox = new RectF();
        getBox(CollisionBox);

        gameScene = GameScene.getTop();

        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(25.f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(Color.YELLOW);
        SoundEffects.get().play(R.raw.click_00, 0);
    }

    private float Value = 0f;

    public void SetScale(float value) {
        Value = value;
    }

    @Override
    public void update() {
        if (IsDead) {
            gameScene.DeleteUiScene();
            IsDead = false;
            releaseTouch();
            this.remove();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    captureTouch();
                    int mx = (int) e.getX();
                    int my = (int) e.getY();

                    TowerBase.TowerLayer TowerState = TowerBase.TowerLayer.COUNT;

                    if (my < y) {
                        tower.UpgradeTower();
                        IsDead = true;
                    }

                    if (my > y) {
                        tower.RemoveTower();
                        // 깃발 생성
                        if(gameScene instanceof FirstStage) {
                            gameScene.getGameWorld().add(FirstStage.Layer.object.ordinal(), new TowerFlag(x,y +UiBridge.y(10) ,0,0, gameScene));
                        } else if(gameScene instanceof SecondStage) {
                            gameScene.getGameWorld().add(SecondStage.Layer.object.ordinal(), new TowerFlag(x,y +UiBridge.y(10) ,0,0, gameScene));
                        }
                        IsDead = true;
                    }

                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                releaseTouch();
                if (this.CollisionBox.contains((int) e.getX(), (int) e.getY())) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.scale(Value, Value, x, y);
        String text = Integer.toString(tower.GetGold());
        super.draw(canvas);
        canvas.drawText(text,x, y -  UiBridge.y(25), paint);
        canvas.restore();
    }
}
