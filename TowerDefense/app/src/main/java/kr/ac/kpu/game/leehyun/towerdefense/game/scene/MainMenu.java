package kr.ac.kpu.game.leehyun.towerdefense.game.scene;

import android.media.MediaPlayer;
import android.util.Log;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.Button;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainTitle;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.WindowChange;

public class MainMenu extends GameScene {
    private static final String TAG = MainMenu.class.getSimpleName();
    private Button StartButton;
    private Button BackButton;
    private MediaPlayer sound;

    public enum Layer {
        bg, button, title, ui, COUNT
    }

    private GameTimer timer;

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        this.sound = MediaPlayer.create(UiBridge.getContext(), R.raw.main_bgm);
        sound.start();
        sound.setLooping(true);
        Initialize();
    }

    @Override
    public void pause() {
        sound.pause();
    }

    @Override
    public void exit() {
        sound.stop();
        sound.release();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        // BackGround
        gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, cy, width, height, R.mipmap.bg_start));

        // Title
        gameWorld.add(Layer.title.ordinal(), new MainTitle(cx, cy - UiBridge.y(100), -65, -65, R.mipmap.title));

        // Button
        StartButton = new Button(cx, cy + UiBridge.y(50), -90, -90,
                R.mipmap.start_normal, R.mipmap.start_touch, R.mipmap.start_press);
        StartButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                AddUiScene(new WindowChange(WindowChange.SceneLayer.Select, WindowChange.StackLayer.Front));
                SoundEffects.get().play(R.raw.button_start, 0);
            }
        });
        gameWorld.add(Layer.button.ordinal(), StartButton);

        BackButton = new Button(cx, cy + UiBridge.y(130), 0, 0,
                R.mipmap.back_normal, R.mipmap.back_touch, R.mipmap.back_press);
        BackButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                GameScene.getTop().onBackPressed();
                SoundEffects.get().play(R.raw.click_01, 0);
            }
        });
        gameWorld.add(Layer.button.ordinal(), BackButton);
    }

    @Override
    public void resume() {
        super.resume();
        sound.start();
        // 버튼 상태 초기화
        StartButton.ButtonState = Button.Layer.normal;
        BackButton.ButtonState = Button.Layer.normal;
    }
}
