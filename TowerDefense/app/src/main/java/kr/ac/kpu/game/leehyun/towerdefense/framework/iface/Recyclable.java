package kr.ac.kpu.game.leehyun.towerdefense.framework.iface;

public interface Recyclable {
    public void recycle();
}
