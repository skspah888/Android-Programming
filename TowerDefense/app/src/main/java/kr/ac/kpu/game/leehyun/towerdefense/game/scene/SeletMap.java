package kr.ac.kpu.game.leehyun.towerdefense.game.scene;


import android.media.MediaPlayer;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.SelectFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui.WindowChange;

public class SeletMap extends GameScene {
    private static final String TAG = SeletMap.class.getSimpleName();
    private static MediaPlayer sound;
    public static boolean stopsound = false;

    public static void StopSound() {
        if(sound != null) {
            sound.stop();
        }
    }

    public enum Layer {
        bg, flag ,ui,  COUNT
    }

    @Override
    protected int getLayerCount() {
        return SeletMap.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        this.sound = MediaPlayer.create(UiBridge.getContext(), R.raw.selectmap_bgm);
        sound.start();
        sound.setLooping(true);
        Initialize();
    }

    @Override
    public void resume() {
        sound.start();
    }

    @Override
    public void pause() {
    }

    @Override
    public void exit() {
        sound.stop();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        // BackGround
        gameWorld.add(SeletMap.Layer.bg.ordinal(), new MainBackground(cx, cy, width, height, R.mipmap.bg_stage));

        // Stage Flag
        SelectFlag Stage01_Flag = new SelectFlag(UiBridge.x(140),height-UiBridge.y(240),150,200,R.mipmap.flag2,7,8);
        Stage01_Flag.Initialize(SelectFlag.FlagState.Stage01);
        gameWorld.add(SeletMap.Layer.flag.ordinal(), Stage01_Flag);


        SelectFlag Stage02_Flag = new SelectFlag(UiBridge.x(320),height-UiBridge.y(210),150,200,R.mipmap.flag2,7,8);
        Stage02_Flag.Initialize(SelectFlag.FlagState.Stage02);
        gameWorld.add(SeletMap.Layer.flag.ordinal(), Stage02_Flag);
    }

    @Override
    public void onBackPressed() {
        AddUiScene(new WindowChange(WindowChange.SceneLayer.Menu, WindowChange.StackLayer.Back));
    }
}
