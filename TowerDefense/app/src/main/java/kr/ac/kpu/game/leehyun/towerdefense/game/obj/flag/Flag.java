package kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag;

import android.graphics.RectF;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.BoxCollidable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Touchable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.AnimObject;

public class Flag extends AnimObject implements Touchable, BoxCollidable {
    protected static final String TAG = Flag.class.getSimpleName();
    protected Runnable onClickRunnable;
    protected RectF CollisionBox;

    public Flag(float x, float y, int width, int height, int resId, int fps, int count) {
        super(x, y, width, height, resId, fps, count);
        CollisionBox = new RectF();
        getBox(CollisionBox);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
       return false;
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
