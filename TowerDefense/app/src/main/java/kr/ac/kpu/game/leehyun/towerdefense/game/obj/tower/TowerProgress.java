package kr.ac.kpu.game.leehyun.towerdefense.game.obj.tower;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.iface.Recyclable;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.RecyclePool;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.bitmap.SharedBitmap;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.FirstStage;

public class TowerProgress extends GameObject implements Recyclable {
    private static final String TAG = TowerProgress.class.getSimpleName();
    private int SoundStream;

    private SharedBitmap FrontProgress;
    private SharedBitmap BackProgress;

    private RectF DrawRect = new RectF();
    private Rect ProgressRect = new Rect();

    private float MaxValue;
    private float Speed;

    private boolean IsFinish = false;


    public TowerProgress(float x, float y) {
        this.x = x;     // 중앙
        this.y = y;

        // 사용할 타워 이미지
        FrontProgress = SharedBitmap.load(R.mipmap.progress_front, false);
        BackProgress = SharedBitmap.load(R.mipmap.progress_back, false);

        ProgressRect.top = (int) (y - FrontProgress.getHeight()/2);
        ProgressRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        ProgressRect.left = (int) (x - FrontProgress.getWidth()/2);
        ProgressRect.right = (int) (x + FrontProgress.getWidth()/2);

        DrawRect.top = (int) (y - FrontProgress.getHeight()/2);
        DrawRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        DrawRect.left = (int) (x - FrontProgress.getWidth()/2);
        DrawRect.right = (int) (x - FrontProgress.getWidth()/2);

        MaxValue = (x + FrontProgress.getWidth() * 0.5f);

        Speed = 50.f;
        IsFinish = false;

        SoundStream = SoundEffects.get().play(R.raw.towerbuild, 0);
    }

    public static TowerProgress get(float x, float y) {
        RecyclePool rpool = GameScene.getTop().getGameWorld().getRecyclePool();
        TowerProgress item = (TowerProgress) rpool.get(TowerProgress.class);
        if (item == null) {
            item = new TowerProgress(x, y);
        } else {
//            item.x = x;
//            item.y = y;
//
//            item.ProgressRect.top = (int) (y - item.FrontProgress.getHeight()/2);
//            item.ProgressRect.bottom = (int) (y + item.FrontProgress.getHeight()/2);
//            item.ProgressRect.left = (int) (x - item.FrontProgress.getWidth()/2);
//            item.ProgressRect.right = (int) (x + item.FrontProgress.getWidth()/2);
//
//            item.DrawRect.top = (int) (y - item.FrontProgress.getHeight()/2);
//            item.DrawRect.bottom = (int) (y + item.FrontProgress.getHeight()/2);
//            item.DrawRect.left = (int) (x - item.FrontProgress.getWidth()/2);
//            item.DrawRect.right = (int) (x - item.FrontProgress.getWidth()/2);
//
//            item.MaxValue = (x + item.FrontProgress.getWidth() * 0.5f);
//
//            item.DrawRect.right = item.DrawRect.left;
//            item.IsFinish = false;
            item.Intialize(x,y);
        }
        return item;
    }

    public void Intialize(float x, float y) {
        this.x = x;     // 중앙
        this.y = y;

        ProgressRect.top = (int) (y - FrontProgress.getHeight()/2);
        ProgressRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        ProgressRect.left = (int) (x - FrontProgress.getWidth()/2);
        ProgressRect.right = (int) (x + FrontProgress.getWidth()/2);

        DrawRect.top = (int) (y - FrontProgress.getHeight()/2);
        DrawRect.bottom = (int) (y + FrontProgress.getHeight()/2);
        DrawRect.left = (int) (x - FrontProgress.getWidth()/2);
        DrawRect.right = (int) (x - FrontProgress.getWidth()/2);

        MaxValue = (x + FrontProgress.getWidth() * 0.5f);

        Speed = 50.f;
        IsFinish = false;
        SoundStream = SoundEffects.get().play(R.raw.towerbuild, 0);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(BackProgress.getBitmap(), ProgressRect, ProgressRect, null);
        canvas.drawBitmap(FrontProgress.getBitmap(), ProgressRect, DrawRect, null);
    }

    public boolean updateRect() {
        // Left -> Right 증가
        if(!IsFinish)
            DrawRect.right +=  Speed * GameTimer.getTimeDiffSeconds();

        if(DrawRect.right > MaxValue) {
            DrawRect.right = MaxValue;
            IsFinish = true;
        }
        return IsFinish;
    }

    public void FinshTower() {
        BackProgress = null;
        FrontProgress = null;
        DrawRect = null;
        ProgressRect = null;
    }

    @Override
    public void recycle() {
    }
}

// 지우는거 : remove()