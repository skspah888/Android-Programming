package kr.ac.kpu.game.leehyun.towerdefense.game.data;

public class GameData {
    public enum GlobalStage {
        MainMenu, SeletMap, FirstStage, SecondStage, COUNT
    }

    public static GlobalStage TestStage = GlobalStage.MainMenu;
    public static boolean Stage01_Open = true;
    public static boolean Stage02_Open = false;

    public static boolean DrawTile = false;

    public static int StageLife = 0;
    public static int StageGold = 0;
    public static int StageCurWave = 0;
    public static int StageMaxWave = 0;

    public static boolean GameResult = false;

    public static void SaveData() {

    }

    public static void LoadData() {

    }
}
