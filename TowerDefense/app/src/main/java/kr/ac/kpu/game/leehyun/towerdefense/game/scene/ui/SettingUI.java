package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.animation.ValueAnimator;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameObject;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.Button;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.OnOffButton;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.data.GameData;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;

public class SettingUI extends GameScene {
    private static final String TAG = SettingUI.class.getSimpleName();
    private Button BackButton;
    private OnOffButton TileButton;
    private MainBackground BackGround;
    private OnOffButton ClearButton;

    public enum Layer {
        bg , ui , COUNT
    }

    @Override
    protected int getLayerCount() {
        return SettingUI.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        Initialize();

        int mdpi_100 = UiBridge.y(100);
        ValueAnimator anim = ValueAnimator.ofFloat(0, mdpi_100);
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float value = (Float) animation.getAnimatedValue();
                scrollTo(value);
            }
        });
        anim.start();
        SoundEffects.get().play(R.raw.click_01, 0);
    }

    private void scrollTo(float y) {
        ArrayList<GameObject> objs = gameWorld.objectsAtLayer(Layer.ui.ordinal());
        int count = objs.size();
        for (int i = 0; i < count; i++) {
            GameObject obj = objs.get(i);
            if(obj != null) {
                float diff = y - obj.getY();
                obj.move(0, diff);
                if(obj instanceof MainBackground)
                    y += UiBridge.y(85);
            }
        }
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        float UiWidth = (float)width * 0.5f;
        float UiHeight = (float)height * 0.8f;

        BackGround = new MainBackground(cx, cy, (int)UiWidth, (int)UiHeight, R.mipmap.bg_slidebutton);
        gameWorld.add(Layer.ui.ordinal(), BackGround);

        // TileView 버튼
        TileButton = new OnOffButton(cx - UiBridge.x(100) , cy + UiBridge.y(85), 0, 0,
                R.mipmap.flag_off, R.mipmap.flag_on, OnOffButton.Type.tile);
        TileButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                GameData.DrawTile = !GameData.DrawTile;
                SoundEffects.get().play(R.raw.click_00, 0);
            }
        });
        gameWorld.add(Layer.ui.ordinal(), TileButton);

        // Stage클리어 버튼
        ClearButton = new OnOffButton(cx  , cy + UiBridge.y(85), 0, 0,
                R.mipmap.lock_off, R.mipmap.lock_on, OnOffButton.Type.stage);
        ClearButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().play(R.raw.click_00, 0);
                DeleteUiScene();
                GameData.Stage02_Open = true;
                new GameResultUI().push();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), ClearButton);

        // Back 버튼
        BackButton = new Button(cx + UiBridge.x(100) , cy + UiBridge.y(85), 0, 0,
                R.mipmap.back2_touch, R.mipmap.back2_touch, R.mipmap.back2_press);
        BackButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().play(R.raw.click_00, 0);
                DeleteUiScene();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), BackButton);
    }

}
