package kr.ac.kpu.game.leehyun.towerdefense.game.obj;

import android.graphics.Canvas;
import android.util.Log;

import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameTimer;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.BitmapObject;

public class MainTitle extends BitmapObject {
    private static final String TAG = MainTitle.class.getSimpleName();
    private float Sign;
    private float Angle;
    private float Speed;
    private float Average;

    public MainTitle(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
        Sign = 1.f;
        Angle = 0.f;
        Speed = 15.f;
        Average = 10.f;
    }

    @Override
    public void update() {
        Angle = Angle + GameTimer.getTimeDiffSeconds() *  Sign * Speed;
        if(Angle > Average)
        {
            Angle = Average;
            Sign = -1f;
        }

        if(Angle < -Average)
        {
            Angle = -Average;
            Sign = 1f;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate(Angle,x,y);
        super.draw(canvas);
        canvas.restore();
    }
}
