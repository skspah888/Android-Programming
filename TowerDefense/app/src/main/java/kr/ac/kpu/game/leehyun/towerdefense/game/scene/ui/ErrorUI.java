package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;

public class ErrorUI extends GameScene {
    private static final String TAG = ErrorUI.class.getSimpleName();
    private final Paint paint;
    private String ErrorText;

    public ErrorUI(String text) {
        ErrorText = text;
        paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(50.f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(Color.WHITE);
    }

    public enum Layer {
        bg , button , text , COUNT
    }

    @Override
    protected int getLayerCount() {
        return ErrorUI.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        Initialize();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, cy, -300, -150, R.mipmap.menubox_2));
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        super.draw(canvas);
        canvas.drawText(ErrorText, UiBridge.metrics.center.x, UiBridge.metrics.center.y + UiBridge.y(10), paint);
        canvas.restore();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            GameScene.getTop().onBackPressed();
            return true;
        }
        return false;
    }
}
