package kr.ac.kpu.game.leehyun.towerdefense.game.scene.ui;

import kr.ac.kpu.game.leehyun.towerdefense.R;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.GameScene;
import kr.ac.kpu.game.leehyun.towerdefense.framework.main.UiBridge;
import kr.ac.kpu.game.leehyun.towerdefense.framework.obj.ui.Button;
import kr.ac.kpu.game.leehyun.towerdefense.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.MainBackground;
import kr.ac.kpu.game.leehyun.towerdefense.game.obj.flag.SelectFlag;
import kr.ac.kpu.game.leehyun.towerdefense.game.scene.SeletMap;

public class StageExplanation extends GameScene {
    private static final String TAG = StageExplanation.class.getSimpleName();
    private boolean IsOpen;
    private SelectFlag.FlagState StageState;
    private Button StartButton;
    private Button BackButton;

    public StageExplanation(SelectFlag.FlagState eState , boolean bOpen) {
        StageState = eState;
        IsOpen = bOpen;
    }

    public enum Layer {
        bg , button , ui , COUNT
    }

    @Override
    protected int getLayerCount() {
        return StageExplanation.Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        Initialize();
    }

    private void Initialize() {
        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int width = UiBridge.metrics.size.x;
        int height = UiBridge.metrics.size.y;

        if(StageState == SelectFlag.FlagState.Stage01) {
            gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, cy, 0, 0, R.mipmap.stage01_explain));

            // Button 생성
            StartButton = new Button(UiBridge.x(190), height - UiBridge.y(80), 0, 0,
                    R.mipmap.start_normal, R.mipmap.start_touch, R.mipmap.start_press);

            if(IsOpen) {
                StartButton.setOnClickRunnable(new Runnable() {
                    @Override
                    public void run() {
                        // Stage01 Scene 생성
                        SoundEffects.get().play(R.raw.button_start, 0);
                        SeletMap.stopsound = true;
                        AddUiScene(new WindowChange(WindowChange.SceneLayer.FirstStage, WindowChange.StackLayer.Front));
                    }
                });
            } else {
                StartButton.setOnClickRunnable(new Runnable() {
                    @Override
                    public void run() {
                        new ErrorUI("아직은 입장할 수 없습니다.").push();
                        SoundEffects.get().play(R.raw.button_start, 0);
                    }
                });
            }
            gameWorld.add(Layer.button.ordinal(), StartButton);
        }



        else if(StageState == SelectFlag.FlagState.Stage02) {
            gameWorld.add(Layer.bg.ordinal(), new MainBackground(cx, cy, 0, 0, R.mipmap.stage02_explain));

            // Button 생성
            StartButton = new Button(UiBridge.x(190), height - UiBridge.y(80), 0, 0,
                    R.mipmap.start_normal, R.mipmap.start_touch, R.mipmap.start_press);

            if(IsOpen) {
                StartButton.setOnClickRunnable(new Runnable() {
                    @Override
                    public void run() {
                        // Stage02 Scene 생성
                        SoundEffects.get().play(R.raw.button_start, 0);
                        SeletMap.stopsound = true;
                        AddUiScene(new WindowChange(WindowChange.SceneLayer.SecondStage, WindowChange.StackLayer.Front));
                    }
                });
            } else {
                StartButton.setOnClickRunnable(new Runnable() {
                    @Override
                    public void run() {
                        new ErrorUI("아직은 입장할 수 없습니다.").push();
                        SoundEffects.get().play(R.raw.button_start, 0);
                    }
                });
            }
            gameWorld.add(Layer.button.ordinal(), StartButton);
        }

        BackButton = new Button(width - UiBridge.x(130), UiBridge.y(70), -130, -130,
                R.mipmap.back2_touch, R.mipmap.back2_touch, R.mipmap.back2_press);
        BackButton.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                GameScene.getTop().onBackPressed();
                SoundEffects.get().play(R.raw.click_01, 0);
            }
        });
        gameWorld.add(Layer.button.ordinal(), BackButton);

    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
        if(IsOpen)
            pop();
    }
}
